"use strict";

function createNewUser() {
	// get a username and a lastname
	const userName = prompt("Enter your name", "Moshe");
	const userSurname = prompt("Enter your surname", "Zukershtein");
	const userBirthDate = prompt("Enter your BirthDate", "dd.mm.yyyy");
	
	// create an object
	const newUser = {
		firstName: userName,
		lastName: userSurname,
		birthDay: userBirthDate,
		
		// returns user Login
		getLogin () {
			return this.firstName.charAt(0).toLowerCase() +
				this.lastName.toLowerCase();
		},
		
		// a setter for this.firstName
		set userName (name) {
			Object.defineProperty(newUser, "firstName", {
				value: name,
				
				//prevent further property direct assignment
				writable: false
			});
		},
		
		// a setter for this.lastName
		set userSurname (surname) {
			Object.defineProperty(newUser, "lastName", {
				value: surname,
				
				//prevent further property direct assignment
				writable: false
			});
		},
			
		
		// returns user's age (only years) 
		getAge() {
			// return year of birth
			let birthYear = 
				new Date(this.birthDay.replace(/\D/g,'.')) //replace any non numerical value with "."
					.getFullYear();
			
			// get present year
			let currYear = new Date().getFullYear();
			
			return currYear - birthYear;
		},
		
		// returns user's age only in years
		getPassword() {
			return this.firstName.charAt(0).toUpperCase() + 
				this.lastName + this.birthDay.substr(-4);
		}
	};
	
	
	// get user's name and surname
	console.log(`HomeTask 4: ${newUser.firstName} ${newUser.lastName}`);
	
	// get user's age
	console.log(`HomeTask 5(a): ${newUser.firstName} ${newUser.lastName} is ${newUser.getAge()} years old`);
	
	// get user's password
	
	console.log(`HomeTask 5(b): ${newUser.firstName}'s password is "${newUser.getPassword()}"`);
}

createNewUser();