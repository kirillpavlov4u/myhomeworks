"use strict";

window.onload = function () {
	const input = document.createElement('INPUT');
	input.id = "input";
	input.type = "number";
	input.style.display = "block";
	input.placeholder = "Price";
	
	let inputValue;
	let checkForSpan;
	let checkForSamp;
	
	function createResult () {
		checkForSpan = document.querySelector("SPAN");
		
		const resultHolder = document.createElement('SPAN');
		resultHolder.textContent = `Текущая цена: ${inputValue}`;
		
		const closeBtn = document.createElement('BUTTON');
		closeBtn.classList.add("close-btn");
		closeBtn.textContent = `X`;
		closeBtn.onclick = deleteResult;
		
		resultHolder.append(closeBtn);
		input.style.color = "greenyellow";
		
		if (checkForSpan === null) {
			document.body.prepend(resultHolder);
		} else {
			checkForSpan.textContent = `Текущая цена: ${inputValue}`;
			checkForSpan.append(closeBtn);
		}
		
		input.value = "";
	}
	
	function falseResult () {
		input.classList.add("negative");
		
		if (!input.nextSibling) {
			const warning = document.createElement('SAMP');
			warning.textContent = "Please enter correct price";
			input.after(warning);
		}
		
		hideWarning(false);
	}
	
	function deleteResult () {
		this.parentNode.remove();
		input.value = "";
	}
	
	function hideWarning (hide) {
		checkForSamp = document.getElementsByTagName('SAMP')[0];
		if (checkForSamp) {
			checkForSamp.hidden = hide;	
		}
	}
	
	input.addEventListener("focus", () => {
		input.classList.add("focused");
	});
	
	input.addEventListener("focusout", () => {
		input.classList.remove("focused");
		inputValue = input.value;
		
		if (inputValue !== "" && inputValue >= 0) {
			createResult();
			input.classList.remove("negative");
			hideWarning(true);
		} else if (checkForSpan && inputValue === "") {
			hideWarning(true);
		} else {
			falseResult();
		}
	});
	
	document.body.append(input);
}



