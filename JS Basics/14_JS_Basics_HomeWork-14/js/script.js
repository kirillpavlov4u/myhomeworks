"use strict";

$(document).ready(() => {
  $(".header-link").click(() => {
    $("html, body").stop(true, false).animate({
        scrollTop: $(event.target.hash).offset().top - 70
    }, 800);
	});
	
	$(window).scroll(() => {
		if ($(window).scrollTop() > $(window).height()) {
			if ($('#scrollButton').length) {
				$("#scrollButton").show();
			} else {
				$("body").append("<button id='scrollButton'>Наверх</button>");
				$("#scrollButton").show();
			}
		}
		
		if ($(window).scrollTop() <= $(window).height()) {
			$("#scrollButton").hide();
		}
		
		$("#scrollButton").click(() => {
			$("html, body").stop(true, false).animate({
				scrollTop:  0
			}, 800);
		});
	});
	
	$("#hotNews").after("<button id='showHide'>Hide</button>");
	
	$("#showHide").click(() => {
		if ($(event.target).text().toLowerCase() === "show") {
			$(event.target).text("Hide");
		} else {
			$(event.target).text("show");
		}
		
		$("#hotNews .grid-container").slideToggle();
	});	
}); // End g