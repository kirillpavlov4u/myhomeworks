"use strict";

window.onload = function () {
	const resumeBtn = document.getElementById("resume");
	const stopBtn = document.getElementById("stop");
	const images = document.getElementsByClassName("image");
	const clocky = document.querySelector(".clock");
	
	let counter = 0;
	let timer = 10;
	let stopWasClicked = 1;
		
	imageOrder(images);

	function imageOrder (collection) {
		timer = 10;
		if (counter === collection.length) {
			collection[collection.length - 1].hidden = true;
			counter = 0;
			collection[counter].hidden = false;
			clockTick(collection[counter]);
		} else if (counter > 0) {
			collection[counter - 1].hidden = true;
			collection[counter].hidden = false;
			clockTick(collection[counter]);
		} else {
			collection[collection.length - 1].hidden = true;
			collection[counter].hidden = false;
			clockTick(collection[counter]);
		}
	}

	function clockTick (img) {
		clocky.textContent = `:${timer}`;
		img.style.animation = "appear 0.5s ease-in";
		
		const ticking = setInterval(() => {
			stopBtn.addEventListener("click", () => {
				stopWatch(ticking);
			});
			timer--;
			clocky.textContent = `:${timer}`;
			if (timer - 0.5 < 0.5){
				img.style.animation = "dissolve 0.5s ease-out";
			}
			if (timer < 1) {
				counter++;
				imageOrder(images);
				clearInterval(ticking);
			}
		}, 1000);
	}
	
	resumeBtn.addEventListener("click", () => {
		tempResume()
	});
	
	function tempResume () {
		if (stopWasClicked) {
			return;
		} else {
			stopWasClicked = 1;
			clockTick(images[counter]);
		}
	}
	
	function stopWatch (id) {
		stopWasClicked = 0;
		clearInterval(id);
	}
	
}