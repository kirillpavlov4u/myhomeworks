"use strict";

let userName = prompt("Gimme your name", "Yizhak");
let userSurname = prompt("Gimme your surname", "Snouden");


const student = {
	name: userName,
	lastName: userSurname,
	table: [],
	
	averageMark() {
		let counter = 0;
		let sum = 0;
		
		for (let i = 0; i <= this.table.length; i++) {
			for (let key in this.table[i]) {
				sum += parseInt(this.table[i][key]);
				counter++;
			}
		}
		
		let average = (sum / counter).toFixed(2);
		
		if (average > 7) {
			return `Студенту назначена стипендия.\n${this.name}'s average mark is - ${average} points. Total is ${sum} points.`
		}
		return `${this.name}'s average mark is - ${average} points. Total is ${sum} points.`
	},
	
	badMarks() {
		let counter = 0;
		
		for (let i = 0; i <= this.table.length; i++) {
			for (let key in this.table[i]) {
				if (this.table[i][key] < 4)
				counter++;
			}
		}
		
		if (!counter) {
			return `Студент переведен на следующий курс.\nBad marks counter is - ${counter}`;
		}
		
		return `Study, you ignoramus!\nBad marks counter is - ${counter}`
	}
}

// Subjects and Marks block

let userSubject = prompt("Enter your study subject", "Math");

// Check userSubject input
while (!userSubject || !isNaN(parseInt(userSubject))) {
	userSubject = prompt("Enter your study subject", userSubject);
}

// The loop will work until a user will press "cancel" button
while(userSubject !== null) {
	let userGrade = prompt(`What grade did you get on ${userSubject}?`, "12");
	
	// Check userGrade input
	while (!userGrade || isNaN(userGrade)) {
		userGrade = prompt(`What grade did you get on ${userSubject}?`, userGrade);
	}
	
	// Add new subject and its mark to the
	// student object, into the table property
	student.table.push({
		[userSubject]: userGrade
	});
	
	// Ask a user for the next subject - continue or break the loop here.
	userSubject = prompt("Enter your study subject", "Math");
}


// A Student Object
console.log(student);

// Outputs
console.log(student.badMarks());
console.log(student.averageMark());
