"use strict";

// get user's fibonacci index number
let userInput = +prompt("Enter some numerical value\nIt should be >= 1 or <=-1", "12");

// check the user's input
while (isNaN(userInput) || !Number.isInteger(userInput) || userInput <= 1 &&
			userInput >= 0) {
	userInput = +prompt(
		"You should enter a number and it should be an integer!\nCome on!", 
		userInput
	);
}


function fiboProgression (f0, f1, n){
	// initiate working variables,
	// avoid original one from being re-written
	let counter = n,
			first = f0,
			second = f1,
			result;
	
	if (counter > -1){
		
		// Because the counter right here is a 
		// positive number, we should decrease it
		// in order to reach the "0" (false) value 
		// and escape the loop.
		
		// Use "--" prefix operator to avoid
		// the result from being one iteration 
		// ahead of needed result
		
		while (Math.abs(--counter)) {
			result = first + second;
			first = second;
			second = result;
		}
	} else {
		
		// Because the counter right here is a 
		// negative number, we should increase it
		// in order to reach the "0" (false) value 
		// and escape the loop.
		
		// Use "++" postfix operator to avoid
		// the result from being one iteration 
		// behind from needed result.
		
		while (Math.abs(counter++)) {
			result = second - first;
			second = first;
			first = result;
		}
	}
	
	// return the  result value with a gentle touch of style
	return `<h1 style="color:deeppink;">The result is: ${result}<h1/>`;
}

// call the function with the result
// is written down right into the html
document.write(
	fiboProgression(0,1, userInput)
);
