"use strict";

window.onload = 
  function () {
    // 'm' variable function, it will be called later and will
    // return a value
    function getUserNumberM () {
      // getting a 'm' number from a user
      let m = prompt("Please, enter a 'm' number.");
      
      // self-invoking function for checking 'm' number input
      (function userNumCheckM() {
        if (m === "" || isNaN(m) === true || (typeof m) === undefined) {
          m = prompt("Please, enter a 'm' number.", m);
          userNumCheckM();
        }

        if (Number.isInteger(+m) === false) {
          m = prompt("Please, enter an INTEGER 'm' number.", m);
          userNumCheckM();
        }

        if (+m < 2) {
          m = prompt("Please, enter a 'm' number which is greater than 1.", m);
          userNumCheckM();
        }
      })();
      
      //returning a 'm' number
      return m;
    };

    // 'n' variable function it will be called later and will
    // return a value
    function getUserNumberN () {
      // getting a 'n' number from a user
      let n = prompt("Please, enter a 'n' number.");
      
      // self-invoking function for checking 'n' number input
      (function userNumCheckN() {
        if (n === "" || isNaN(n) === true || (typeof n) === undefined) {
          n = prompt("Please, enter a 'n' number.", n);
          userNumCheckN();
        }

        if (Number.isInteger(+n) === false) {
          n = prompt("Please, enter an INTEGER 'n' number.", n);
          userNumCheckN();
        }

        if (+n < 2) {
          n = prompt("Please, enter a 'n' number which is greater than 1.", n);
          userNumCheckN();
        }
      })();
      //returning a 'n' number
      return n;
    };
    
    // from here functions are called and here we store
    // our 'm' and 'n' values.
    // To the end of a program, they will remain
    // constant and will not be changed.
    const m = getUserNumberM();
    const n = getUserNumberN();
    
    // innitializing a loop. The Start point and The End point of a loop
    // are defined by 'm' and 'n' variables, which we get from a user
    for (let i = +m; i <= +n; i++) {
      
      // if our range starts from 2 - pass it through to the console
      if (i == 2) {
          console.log(i);
        } else {
          
          // innitializing an array for storing remainder
          // from the while loop
          let results = [];
          
          // the while loop will start from the smallest
          // prime number - 2. 'k' number will be our divider
          let k = 2;
          
          // The while loop will run until the divider
          // will reach current number 'i' value 
          while (k < i) {
            
            // add all remainders from dividing current number 'i'
            // by divider 'k' to the 'results' array
            results.push(i % k);
            k++;
            }
          
          // creating a checking function for each element of the 
          // 'results' array.
          // It will check every value stored in the 'results' array
          // and will return true if condition is satisfied
          function checkForRemainder (remainder) {
            return remainder == 0;
          }
          
          // if no remainder will be equal to the value of '0'
          // than display in the console the current number 'i'.
          // Basically it means our current number is a prime number,
          // beacuse it can not be divided without a remainder, 
          // which is greater than '0' with any
          // divider, that less than itself.
          if (!(results.some(checkForRemainder))) {
            console.log(i);
          }
        }
      }
    }