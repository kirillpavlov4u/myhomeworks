"use strict";

window.onload = 
  function () {
    let userName = prompt("Please, enter your name.", "Ralf");
    let userAge = prompt("Please, specify your age.", "15");

  // checking userName input for error
  function nameInputCheck () {
    if (userName === "" || isNaN(userName) === false || typeof(userName) === null) {
      userName = prompt("Please, enter your name.", userName);
      return nameInputCheck(userName);
      }
    }
    nameInputCheck();
  
  
  function ageInputCheck () {
    if (userAge === "" || isNaN(userAge) === true || typeof(userAge) === null) {
      userAge = prompt("Please, specify your age.", userAge);
      return ageInputCheck(userAge);
      }
    }
    ageInputCheck(); 


  if (parseInt(userAge) < 18) {
    alert("You are not allowed to visit this website.");
  } else if (parseInt(userAge) >= 18 && parseInt(userAge) <= 22){
    // We use const here under asumption that output of a modal window
    // will not/can not be changed further
    const PROCEED_PAGE_LOAD = confirm("Are you sure you want to continue?");

    if (PROCEED_PAGE_LOAD === true) {
      alert("Welcome, " + userName + ".");
    } else {
      alert("You are not allowed to visit this website.");
    }
  } else {
    alert("Welcome, " + userName + ".");
  }
}