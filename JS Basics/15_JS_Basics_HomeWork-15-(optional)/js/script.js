"use strict";


// ask for an input
let userInput = +prompt("Enter some numerical value", "12");


// check an input and if some/all conditions is/are true
// ask user for an input again
while (isNaN(userInput) || userInput < 0 || !Number.isInteger(userInput)) {
	userInput = +prompt(
		"You should enter a number and it should be an integer!\nCome on!", 
		userInput
	);
}

// create function to check input and calculate factorial
function factorial () {
	
	// since the minimum factoial may only be 1
	// our result variable will hold this value by default
	let result = 1;
	
	// factorial for 1 && 0 is 1, so we just return the result variable
	if (userInput == 0 || userInput == 1) {
		return(result);
	}
	
	// if our input is greater than 1 || 0 
	// this self-invoking function will be called
	(function calcResult () {
		
		// as long as userInput greater than 1
		// our recursion should repeat iterations
		if (userInput > 1) {
			result *= (userInput--);
			calcResult();
		}
	})();
	
	return result;
}

console.log(factorial());