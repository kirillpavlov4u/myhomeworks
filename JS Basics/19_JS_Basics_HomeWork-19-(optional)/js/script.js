"use strict";


// Initiate team, team members and story points

// NOTE: as far as your team's daily speed is 
// bigger than 8, you will always be ahead of schedule. 

const oneDaySpeed = [
	{
		name: "John",
		storyPointSpeed: 5
	},
	{
		name: "Isabella",
		storyPointSpeed: 3
	},
	{
		name: "Matthew",
		storyPointSpeed: 2
	},
	{
		name: "Jacob",
		storyPointSpeed: 1
	},
	{
		name: "Mason",
		storyPointSpeed: 6
	},
	{
		name: "Samantha",
		storyPointSpeed: 7
	},
	{
		name: "Victoria",
		storyPointSpeed: 13
	},
	{
		name: "Logan",
		storyPointSpeed: 2
	},
	{
		name: "Ryan",
		storyPointSpeed: 1
	},
	{
		name: "Zoey",
		storyPointSpeed: 0.5
	}
];

// Initiate project.

const storyPoints = [
	{
		hours: 195,
		task: "clientBriefing"
	},
	{
		hours: 1540,
		task: "brainstorming"
	},
	{
		hours: 100,
		task: "puttingTasksInOrder"
	},
	{
		hours: 89,
		task: "processInitializing"
	},
	{
		hours: 13,
		task: "workPreparations"
	},
	{
		hours: 1115,
		task: "documentationReading"
	},
	{
		hours: 150,
		task: "problemSolving"
	},
	{
		hours: 2988,
		task: "coding"
	},
	{
		hours: 345,
		task: "codeTesting"
	},
	{
		hours: 300,
		task: "reporting"
	},
	{
		hours: 20,
		task: "presenting"
	},
];



function countProjectDays (team, tasks) {
	
	// Client/Project side data
	
	const START_DATE = Date.now();
	const TIME_FOR_TASK = tasks.reduce(
		(total, item) => {return total + item.hours;}, 
	0);
	
	const WORK_DAYS =	parseInt(TIME_FOR_TASK / 24);
	const REAL_DAYS = WORK_DAYS + deadLine(START_DATE, WORK_DAYS);
	
	// Team side data
	
	const DAILY_SPEED = team.reduce(
		(total, item) => {return total + item.storyPointSpeed;}, 
	0);
	const PROJECT_DAYS = TIME_FOR_TASK / DAILY_SPEED;
	const REAL_PROJECT_DAYS = parseInt(PROJECT_DAYS + deadLine(START_DATE, PROJECT_DAYS));
	
	// Behind or ahead of schedule
	
	const SCHEDULE = REAL_PROJECT_DAYS - REAL_DAYS;
	
	if (REAL_PROJECT_DAYS > REAL_DAYS) {
		const HOURS_BEFORE_SCHEDULE = parseInt((SCHEDULE - deadLine(START_DATE, SCHEDULE)) * 8);
		return `Команде разработчиков придется потратить дополнительно ${HOURS_BEFORE_SCHEDULE} рабочих часов после дедлайна, чтобы выполнить все задачи в беклоге.`;
	} else {
		const AHEAD_OF_SCHEDULE = Math.abs(SCHEDULE) - deadLine(START_DATE, Math.abs(SCHEDULE));
		return `Все задачи будут успешно выполнены за ${AHEAD_OF_SCHEDULE} рабочик дней до наступления дедлайна!`;
	}
}

// Function for counting extra weekend days

function deadLine (from, totalDays) {
	let DAY_MILLISECONDS = 24 * 60 * 60 * 1000;
	let weekdays = 0;

	for (let i = 0; i < totalDays; i++) {
		let daysCounter = from + i * DAY_MILLISECONDS;
		if (new Date(daysCounter).getDay() == 0 || 
				new Date(daysCounter).getDay() == 1) {
			weekdays++;
		}
	}
	return weekdays;
}

console.log(countProjectDays(oneDaySpeed, storyPoints));



































