"use strict";

document.addEventListener("DOMContentLoaded", () => {
	const clacdisplay = document.getElementById("calcDisplay");
	const inputParent = document.getElementById("calcDisplay").parentElement;
		
	let inputValue = "";
	let memoryInput = 0;
	let operationsSet = []; 
	let resultWasShowed = false;
	
	let eraseMemory = false;
	let memoryOpts = false;
	
	document.addEventListener('click', (event) => {		
		if (!event.target.value) {
			return;
		}
		
		if (event.target.value.match(/^[0-9.]/)) {
			setInput(event.target.value);
		}
		
		if (event.target.value.match(/^[-+*\/=]/)) {
			operation(event.target.value);
		}
		
		if (event.target.id.includes("memory")) {
			memoryOperations(event.target.value);
		}
		
		if (event.target.id === "clearInput") {
			clear();
		}
	});
	
	document.addEventListener('keyup', (event) => {
		if (event.code === "Enter") {
			showResult();
		}
	})
	
	function setInput (symb) {
		if (memoryOpts) {
			inputValue = "";
			memoryOpts = false;
		}
		
		memoryOperDefaults();
		
		if (resultWasShowed) {
			inputValue = "";
			operationsSet = [];
			resultWasShowed = false;
		}
		
		if (clacdisplay.value === "0" && symb.match(/^[0-9]/)) {
			clacdisplay.value = symb;
			inputValue = symb;
		} else if (symb.match(/^[0-9]/)) {
			inputValue += symb;
		}
		
		if (symb.match(/^[.]/) && !inputValue.includes(".")) {
			inputValue += symb;
		}
		
		if (clacdisplay.value === "0" || !clacdisplay.value && symb === ".") {
			inputValue = "0" + symb;
		}
		
		if (clacdisplay.value === "0" && symb === "0") {
			inputValue = "0";
		}
		
		clacdisplay.value = inputValue;
	}
	
	function operation (oper) {
		memoryOperDefaults();
		
		if (inputValue !== "" && oper !== "=") {
			operationsSet.push(inputValue);
			operationsSet.push(oper);
			inputValue = "";
		}
		
		if (resultWasShowed && oper !== "=") {
			operationsSet.push(oper);
			inputValue = "";
			resultWasShowed = false;
		}
		
		if (operationsSet.length === 0 && oper === "-") {
			operationsSet.push(oper);
		}
		
		if (oper === "=") {
			showResult();
		}
	}
	
	function memoryOperations (memo) {		
		if (memo === "m+") {
			memoryInput += parseInt(clacdisplay.value);
			memoryDefaultSettings();
		}
		
		if (memo === "m-") {
			memoryInput -= parseInt(clacdisplay.value);
			memoryDefaultSettings();
		}
		
		if (memo === "mrc" && eraseMemory) {
			memoryInput = 0;
			inputParent.classList.remove("memory-symb");
			eraseMemory = false;
		} else if (!eraseMemory && memo === "mrc") {
			eraseMemory = true;
			memoryOpts = true;
			clacdisplay.value = memoryInput;
			inputParent.classList.add("memory-symb");
		}
	}
	
	function showResult () {
		memoryOperDefaults();
		
		let result;
		if (!clacdisplay.value && !operationsSet.length) {
			return;
		}
		
		if (clacdisplay.value && !inputValue && !operationsSet.length) {
			inputValue = clacdisplay.value;
			console.log(inputValue)
			operationsSet.push(inputValue);
		}
		
		if (inputValue) {
			operationsSet.push(inputValue);
			clacdisplay.value = "";
			inputValue = "";
		}
		result = eval(operationsSet.join(""));
		
		if (result.toString().includes(".")) {
			result = result.toFixed(2);
		}
		
		operationsSet = [result];
		clacdisplay.value = result;
		resultWasShowed = true;
	}
	
	function clear () {
		memoryOperDefaults();
		eraseMemory = false;
		inputValue = "";
		operationsSet = [];
		clacdisplay.value = "0";
	}
	
	function memoryOperDefaults () {
		eraseMemory = false;
		inputParent.classList.remove("memory-symb");
	}
	
	function memoryDefaultSettings () {
		inputParent.classList.add("memory-symb");
		eraseMemory = false;
		memoryOpts = true;
	}
});