"use strict";

const dataTypesArray = [{item: 3}, 23, "Blah-blah", true, Symbol(),  Array.prototype.forEach, undefined, null, [1,2,3,4,5], {item: 3}, 23, "Blah-blah", true, Symbol(),  Array.prototype.forEach, undefined, null, [1,2,3,4,5]];


function filterArray (arr, dataType) {
	let resultArr = arr.map((item) => item);
	
	function returnIfObject (elem) {
		if (elem !== null && elem !== undefined)
			if (elem.constructor === Object) {
				return elem;
		}
	}
	
	function deleteItem (elem, array) {
		array.splice(array.indexOf(elem), 1)
	}
	
	(function iterate () {
		for (let i = 0; i < resultArr.length; i++) {
			let currItem = resultArr[i];
			
			if (dataType.toLowerCase() === "object") {
				if (returnIfObject(currItem) !== undefined) {
					deleteItem(currItem, resultArr);
					iterate();
				}
			} else if (dataType.toLowerCase() === "array") {
				if (Array.isArray(currItem)) {
					deleteItem(currItem, resultArr);
					iterate();
				}
			} else if (dataType.toLowerCase() === "undefined") {
				if (currItem === undefined) {
					deleteItem(currItem, resultArr);
					iterate();
				}
			} else if (dataType.toLowerCase() === "null") {
				if (currItem === null) {
					deleteItem(currItem, resultArr);
					iterate();
				}
			} else if (dataType.toLowerCase() === typeof currItem) {
				deleteItem(currItem, resultArr);
				iterate();
			}
		}
	})();
	
	return resultArr;
}

// You may perform tests on further data types:
// undefined, null, object, array, boolean, function, number, string
// symbol
console.log(filterArray(dataTypesArray, "undefined"));

