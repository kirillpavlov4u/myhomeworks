"use strict";

const styleSheet = document.getElementById("changable");
const button = document.getElementById("life-changer");
const styleHref = 'style/styles.css';
const nextStyleHref = 'style/next_style.css';

if (localStorage.getItem("theme")) {
	styleSheet.href = localStorage.getItem("theme");
} 

button.addEventListener("click", checkStyle);

function checkStyle () {
	const href = styleSheet.href;
	
	if (href.includes(nextStyleHref)) {
		styleSheet.href = styleHref;
	} else {
		styleSheet.href = nextStyleHref;
	}
	
	localStorage.setItem("theme", styleSheet.href);
}