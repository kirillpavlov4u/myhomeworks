"use strict";

const form = document.getElementsByTagName("FORM")[0];
const inputs = document.querySelectorAll("INPUT");
const labels = document.querySelectorAll("LABEL");

form.addEventListener('click', changeCaller);
form.addEventListener('submit', checkPasswords);

function changeCaller (event) {
	const caller = event.path[0].nodeName;
	if (caller === "I") {
		changeIcon(event);
	} 
	
	if (caller === "INPUT") {
		changeInputType(event.path[0], true);
	}
}

function changeIcon (event) {
	event.path.forEach(item => {
		if (item.nodeName === "I") {
			item.classList.toggle('fa-eye');
    	item.classList.toggle('fa-eye-slash');
			
			changeInputType(item);
		}
	});
}

function changeInputType (elem, procedure) {	
		if (procedure) {
			const iconStatus = elem.nextElementSibling.classList.contains('fa-eye-slash');
			
			if (iconStatus) {
				elem.setAttribute("type", "text");
			}
		} else {
			const input = elem.previousElementSibling;
			const changeType = input.getAttribute("type") === "password" ? "text" : "password";
			input.setAttribute("type", changeType);
		}
}

function checkPasswords (event) {
	event.preventDefault();
	
	if (!document.querySelector("#warning")) {
		createWarningElem();
	}
	
	if (inputs[0].value !== inputs[1].value) {
		warning.textContent = showWarning();
		warning.hidden = false;
	} else if (inputs[0].value === "" && inputs[1].value === "") {
		warning.textContent = showWarning(true);
		warning.hidden = false;
	} else { 
		warning.hidden = true;
		setTimeout(() => alert("You are welcome!"), 0);
	}
}

function showWarning (empty) {
	if (empty) {
		return "Введите пароль";
	} else {
		return "Нужно ввести одинаковые значения";
	}
}

function createWarningElem () {
	const warning = document.createElement('P');
	warning.id = "warning";
	labels[1].append(warning);
	warning.hidden = true;
}
