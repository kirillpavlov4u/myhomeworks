"use strict";

// Didn't have any inspiration to create smth meaningful

const VEHICLES = [
	{
		blah: "BLAH",
		locales: {
			name: "Model en_US Toyota Prius", 
			description: "Hybrid car en_US Toyota",
			contentType: {name: "Alcantara en_US Toyota"}
		},
		name: "Toyota",	
		contentType: {name: "Bright Alcantara en_US"},
		description: "Boring car en_US",
	}
];

function filterCollection (arr, keywords, strictSort, ...properties) {
	let resultArr = [];
	const keys = keywords.split(" ");
	let nodesPropValue =[];
	let nestingLevelCounter = 0;
	
	function byStrictSort (str) {
		const keysMatch = keys.map(
			item => {
				return str.toLowerCase().includes(item.toLowerCase());
			}
		);
		
		if (strictSort) {
			if (keysMatch.every(item => {return item === true})) {
				return str;
			}
		} else {
			if (keysMatch.some(item => {return item === true})) {
				return str;
			}
		}
	}
	
	function searchProperty (value, prop) {
		for (let i = 0; i < properties.length; i++) {
			if (`${prop}` === properties[i]) {
				if (byStrictSort(value) !== undefined) {
						nodesPropValue.push("." + prop);
						nodesPropValue.push(byStrictSort(value));
						resultArr.push(nodesPropValue);
					
						if (nestingLevelCounter > 0) {
							nodesPropValue = [nodesPropValue[0]];
						} else {
							nodesPropValue = [];
						}
					}
				}
			}
		}
	
	function searchObject (obj) {
		for (let key in obj ) {
			if (obj[key].constructor === Object) {
				nodesPropValue.push("." + [key]);
				nestingLevelCounter++;
				searchObject(obj[key]);
				nodesPropValue = [];
				nestingLevelCounter = 0;
			} else {
				searchProperty(obj[key], [key]);
			}
		}
	}
	
	arr.forEach(item => {
		if (item.constructor === Object) {
			searchObject(item);
		}
	});
	
	
	if (resultArr.length > 0) {
		return resultArr;
	} else {
		return "No results found";
	}
	
}


console.log(filterCollection (VEHICLES, 'en_US Toyota', true /* true */, 'name', 'description', 'contentType.name', 'locales.name', 'locales.description'));