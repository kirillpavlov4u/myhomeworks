"use strict";

const newTable = document.createElement("TABLE");
const tableBody = document.createElement("TBODY");

for (let k = 0; k < 30; k++) {
	const tr = tableBody.insertRow(-1);
	
	for (let i = 0; i < 30; i++) {
		const td = tr.insertCell(-1);
	}
}

newTable.addEventListener("click", selectCell);
document.addEventListener("click", reverseColors);

function selectCell (event) {
	if (event.target.nodeName === "TD") {
		toggleClass(event.target);
	}
}

function reverseColors (event) {
	const cells = document.querySelectorAll("TD");
	
	if (event.target.nodeName === "BODY" ||
		 event.target.nodeName === "HTML") {
		cells.forEach(cell => {
			return toggleClass(cell);
		});
	}
}

function toggleClass (elem) {
	return elem.classList.toggle("black");
}

newTable.append(tableBody);
document.body.append(newTable)