"use strict";

const initialBtn = document.getElementById("init-btn");
let radius;

initialBtn.addEventListener("click", createDrawBtn);

function createInput () {
	const input = document.createElement("INPUT");
	input.placeholder = "Input radius";
	input.type = "number";
	input.id  = "radius-input";
	
	input.addEventListener('input', setRadius);
	
	document.body.childNodes[0].remove();
	document.body.childNodes[0].replaceWith(input); 
}


function setRadius () {
	radius = this.value;
	
	if (radius < 0) {
		radius = this.value * -1;
	}
}

function createDrawBtn () {
	createInput();
	
	const newBtn = document.createElement("BUTTON");
	newBtn.textContent = "Нарисовать";
	
	newBtn.addEventListener("click", () => {
		createHundredCircles();
	});
	
	document.body.childNodes[1].replaceWith(newBtn); 
}

function createHundredCircles () {
	document.body.style.width = `${(radius * 2 + 5) * 10}px`;
	document.body.style.height = `${(radius * 2 + 5) * 10}px`;
	document.body.style.justifyContent = `flex-start`;

	
	let circles = document.createDocumentFragment();
	
	for (let i = 1; i <= 100; i++) {
		const newCircle = document.createElement("DIV");
		newCircle.style.width = `${radius * 2}px`;
		newCircle.style.height = `${radius * 2}px`;
		newCircle.style.backgroundColor = `rgba(${createColor()},${createColor()},${createColor()},${Math.random() + 0.4})`;
		
		if (i < 10) {
			newCircle.classList.add("circle",`00${i}`);
			circles.appendChild(newCircle);
		} else if (i < 100) {
			newCircle.classList.add("circle",`0${i}`);
			circles.appendChild(newCircle);
		} else {
			newCircle.classList.add("circle",`${i}`);
			circles.appendChild(newCircle);
		}
	}
	
	document.body.addEventListener('click', () => {
		if(event.target.classList.contains("circle")) {
			deleteCircle(event.target);
		}
	});
	
	document.body.childNodes[0].remove();
	document.body.childNodes[0].replaceWith(circles);
}

function createColor () {
	return Math.floor(Math.random() * 235);
}

function deleteCircle (target) {
	const allCircles = document.body.childNodes;
	
	for (let i = 0; i < allCircles.length; i++) {
		if (allCircles[i].classList[1] == target.classList[1]) {
			document.body.removeChild(allCircles[i]);
			break;
		}
	}
}
