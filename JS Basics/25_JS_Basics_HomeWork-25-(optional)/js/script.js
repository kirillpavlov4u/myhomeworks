"use strict";

document.addEventListener('DOMContentLoaded', () => {
	const carousel = document.getElementById("carousel"),
				images = carousel.childElementCount;

	const imgWindowWidth = document.getElementById("imageWindow").offsetWidth;	
	
	const firstImg = carousel.children[0].cloneNode(true);
	carousel.append(firstImg);	
	const lastImg = carousel.children[images - 1].cloneNode(true);
	carousel.prepend(lastImg);
	
	carousel.childNodes.forEach(img => {
		if(img.nodeName === "IMG")
		img.title = img.alt;
	});

	initialImg();
	
	function initialImg () {
		carousel.style.left = `-${imgWindowWidth}px`;
	}
	
	function initialImgReverse () {
		carousel.style.left = `-${imgWindowWidth * 6}px`;
	}
	
	let currentImg = 0;

	document.addEventListener("click", (event) => {
		if(event.target.id === "previous") {
			scrollPreviousImage();
		}
		
		if(event.target.id === "next") {
			scrollNextImage();
		}
	});
	
	function scrollPreviousImage () {
		currentImg--;
		carousel.style.transition = "left 0.5s";
		if (!carousel.style.left) {
			carousel.style.left = `${imgWindowWidth}px`; 
		} else {
			carousel.style.left = parseInt(carousel.style.left) + imgWindowWidth + "px";
		}
		
		if (currentImg === -1) {
			resetCarouselReverse();
			currentImg = 5;
		}
	}
	
	function scrollNextImage () {
		carousel.style.transition = "left 0.5s";
		if (!carousel.style.left) {
			carousel.style.left = `-${imgWindowWidth}px`; 
		} else {
			carousel.style.left = parseInt(carousel.style.left) - imgWindowWidth + "px";
		}
		
		if (currentImg === 5) {
			resetCarousel();
			currentImg = 0;
		}	else {
			currentImg++;
		}		
	}
	
	function resetCarousel () {		
		setTimeout(resetImg, 350);
		
		function resetImg () {
			carousel.style.transition = "";
			initialImg();
		}
	}
	
	function resetCarouselReverse () {		
		setTimeout(resetImg, 350);
		
		function resetImg () {
			carousel.style.transition = "";
			initialImgReverse();
		}
	}
});


