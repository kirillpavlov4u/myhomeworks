"use strict";


// Just an ordinary Joe
const person = {
  firstName: "John",
  lastName: "Doe",
  age: 50,
	divorsed: false,
	isGoodMan: true,
	sex: "male",
  eyeColor: "blue",
	family: {
		maleParent: "George",
		femalePArent: "Emily",
		spouce: {
			sex: "female",
			age: 34,
			children: {
				son: "Eddie",
				daughter: {girl: "Loise"}
			}
		},
			
		children: [
				{son: "Eddie"},
				{daughter: "Loise"}
			]
	},
	occupations: [
		"fireman",
		"office worker",
		"handyman",
		"CEO",
		"computer engineer",
		"film director",
		{workingStatus: "retired"}
	],
	isRetired () {
		return (this.age - 65) >= 0;
	},
	creditHistory: [
		{
			firstCredit: 5000,
			purpose: "Need to please my wife",
			status: "repaid"
		},
		{
			secondCredit: 10000,
			purpose: 'First child was born',
			status: "repaid"
		},
		{
			thirdCredit: 1000000,
			purpose: "Private matter",
			status: "Was not repaid. We never heard about our client again"
		}
	]
};

// Innitiatin a clone Object.
const copyPerson = makeClone(person);


// Actual cloning function

function makeClone (object, copy){
	const result = {};
	typeCheck(object, result);
	
	function typeCheck (checking, output) {
		
		for (let key in checking) {
			if (checking[key].constructor === Array) {
				output[key] = cloneArray(checking[key]);
			} else if (checking[key].constructor === Object) {
				output[key] = cloneObject(checking[key]);
			} else {
				
				// The level where all of the cloning is happening
				
				output[key] = checking[key];
			}
		}
	}
	
	// Copy Array
	
	// Note: At first I thought I have to build a function
	// with the loop inside it in order to
	// iterate through array's items and perform further actions.
	// But the function works without any loop. Pure magic.
	
	function cloneArray (from) {
		const output = [];
		typeCheck(from, output);
		
		return output;
	}
	
	// Copy Objects
	function cloneObject (from) {
		const output = {};
		typeCheck(from, output);
		
		return output;
	}
	

	return result;
}


// Check, if our brand new clone does not have
// any refferences on any level of nested property

function checkReference (object) {
	
	let counter = 0;
	
	// Adding some fun part.
	
	let primitives = 0
	let arrays = 0;
	let objects = 0;
	typeCheck(object);
	
	function typeCheck (checking, output) {
		
		for (let key in checking) {
			if (checking[key].constructor === Array) {
				cloneArray(checking[key]);
			} else if (checking[key].constructor === Object) {
				cloneObject(checking[key]);
			} else {
				if (checking.hasOwnProperty([key]) === false) {
					counter++;
				}
				primitives++;
			}
		}
	}
	
	// Check if Array and count array
	function cloneArray (from) {
		typeCheck(from);
		arrays++;
	}
	
	// Check if Objects and count object
	function cloneObject (from) {
		typeCheck(from);
		objects++;
	}
	

	return  `${counter ? true : false}\n Arrays: ${arrays},\n Objects: ${objects},\n Primitives: ${primitives}`
}



console.log("\nAn original:");
console.log(person);
console.log("\nA copy:");
console.log(copyPerson);
console.log("\n Checking for nested references.\n Returns false if any is found.");
console.log(checkReference(copyPerson));



