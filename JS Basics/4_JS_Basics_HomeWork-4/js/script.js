"use strict";

function createNewUser() {
	// get a username and a lastname
	const userName = prompt("Enter your name", "Moshe");
	const userSurname = prompt("Enter your surname", "Zukershtein");
	
	// create an object
	const newUser = {
		firstName: userName,
		lastName: userSurname,		
		getLogin () {
			return this.firstName.charAt(0).toLowerCase() +
				this.lastName.toLowerCase();
		}
	};
	
	// create a setter for firstName property
	Object.defineProperty(newUser, "userName", {
		set: function (name) {
			Object.defineProperty(newUser, "firstName", {
				value: name,
				writable: false
			});
		}
	});
	
	// create a setter for lastName property
	Object.defineProperty(newUser, "userSurname", {
		set: function (surname) {
			Object.defineProperty(newUser, "lastName", {
				value: surname,
				writable: false
			});
		}
	});
	
	console.log(`Assigned by prompts: ${newUser.getLogin()}`); // returns prompts name and lastname
	
	// Modify name and surname through setters
	newUser.userName = "Bob"; // assignes Bob
	newUser.userSurname = "Zimmerman"; // assignes Zimmerman
	console.log(`Changed through setters: ${newUser.getLogin()}`); // returns bzimmerman
	
	/*
	
	// Modify name and surname through an assigment operator
	newUser.firstName = "Bob"; // gives an error
	newUser.lastName = "Zimmerman"; // do not proceeds
	console.log(newUser.getLogin()); // do not proceeds
	
	*/
}

createNewUser();