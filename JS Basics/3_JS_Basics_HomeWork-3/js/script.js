"use strict";

window.onload = 
  function () {
  
    // Here we set and store our First, 'X', Number
    const firstUserNum = 
      (function () {
        let numX = prompt("Please, enter an 'x' number."); 
        
          (function inputCheck () {
            if (numX.match(/[0-9]/) === null || numX.match(/[a-zA-Z ]/) !== null || numX === '' || typeof numX === 'undefined' || numX == null || Number.isNaN(numX) == true) {
              numX = prompt("Please, enter an 'x' number.", numX);
              inputCheck();
            }
          })();
        
        return numX;
      })();
  
    // Here we set and store our Operator
    const userOperator = 
      (function () {
        let operator = prompt("Please, enter an operator: + , - , * or / ."); 
        
          (function inputCheck () {
            if ( operator.match(/[*+-/]/) === null || operator.match(/[a-zA-Z0-9 ]/) !== null || operator === '' || typeof operator === 'undefined' || operator == null) {
              operator = prompt("Please, enter an operator: + , - , * or / .", operator);
              inputCheck();
            }
          })();
        
        return operator;
      })();
  
    // Here we set and store our Second, 'Y', Number
    const secondUserNum = 
      (function () {
        let numY = prompt("Please, enter an 'y' number."); 
        
          (function inputCheck () {
            if (numY.match(/[0-9]/) === null || numY.match(/[a-zA-Z ]/) !== null || numY === '' || typeof numY === 'undefined' || numY == null || Number.isNaN(numY) == true) {
              operator = prompt("Please, enter an 'y' number.", numY);
              inputCheck();
            }
          })();
        
        return numY;
      })();
  
    // Calculator function, which receives as arguments First X-Number, 
    // Second Y-Number and operator 
    function calculateUserInput (x, y, operator) {
      let result;

      if (operator === '+') {
        result = parseFloat(x) + parseFloat(y);
      } else if (operator === '-') {
        result = parseFloat(x) - parseFloat(y);                    
      } else if (operator === '*') {
        result = parseFloat(x) * parseFloat(y); 
      } else if (operator === '/') {
        result = parseFloat(x) / parseFloat(y);          
      }

      return result;
    }

    // inside .log() method we call our Calculator function and passing
    // our arguments inside. Than we may see our result in the Console window
    console.log(
      calculateUserInput(firstUserNum,secondUserNum,userOperator)
    );
}