"use strict";

const tabsContainer = document.getElementsByClassName('centered-content')[0];
const tabs = document.getElementsByClassName('tabs')[0];
const tabsTextContainer = document.getElementsByClassName('tabs-content')[0];
const textElements = tabsTextContainer.childNodes;

tabs.addEventListener('click', selectTab);

function selectTab (event) {
	const tabNodesArr = [...tabs.childNodes].forEach(tab => {
		const classes = tab.classList;
		
		if (tab.nodeName === "LI" && classes.contains("active")) {
			return classes.remove("active");
		}
	});
	
	event.path.find(tab => {
		if (tab.nodeName === "LI" && tab.classList.contains("tabs-title")) {
			return tab.classList.add("active");
		}
		
	});
	
	
	showText();
}

function showText () {
	const currentSelection = document.getElementsByClassName('active')[0];
	const currentSelectorText = currentSelection.textContent;
	
	for (let i = 0; i < textElements.length; i++) {
		if (textElements[i].nodeName === 'LI') {
			textElements[i].style.display = "none";
		}
	}
	
	for (let i = 0; i < textElements.length; i++) {
		if(textElements[i].nodeName === '#comment') {
			if (textElements[i].data.includes(currentSelection.textContent)) {
				textElements[i + 2].style.display = "";
			}
		}
	}
}
showText();