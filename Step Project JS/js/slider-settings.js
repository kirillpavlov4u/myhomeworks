$(document).ready(function(){
	 $('.current-slide-wrapper').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider'
	});
	$('.slider').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		asNavFor: '.current-slide-wrapper',
		focusOnSelect: true,
		prevArrow: `<button type="button" class="slick-btn slick-prev"><i class="fas fa-chevron-left"></button>`,
		nextArrow: `<button type="button" class="slick-btn slick-next"><i class="fas fa-chevron-right"></button>`,
		centerMode: true,
		centerPadding: '10px'
	});
});