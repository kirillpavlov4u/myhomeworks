$('.gallery-container').masonry({
  // options
  itemSelector: ".gallery-img",
  columnWidth: '.grid-sizer',
	gutter: 20,
	horizontalOrder: true
});