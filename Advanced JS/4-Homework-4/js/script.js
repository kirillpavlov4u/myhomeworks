"use strict";

const allMovies = getStarWarsMovies(6);

function getStarWarsMovies (episodes) {
  const result = [];

  for (let i = 1; i <= episodes; i++) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        result.push(JSON.parse(this.responseText));

        if (this.readyState == 4 && this.status == 200 && episodes == i) {
          showMovies(result, "episode_id", "title", "opening_crawl");
          loadCharacters(result, "characters");
        }
      }    
    };
    xhttp.open("GET", `https://swapi.dev/api/films/${i}`, true);
    xhttp.send();
  }

  return result;
}

function showMovies (arr, ...args) {
  arr.forEach((movie) => {
    const container = document.createElement("DIV");
    container.classList.add(movie.title.toLowerCase().split(' ').join('-'));

    for (let p = 0; p < args.length; p++) {
      const text = document.createElement("P");
      text.innerText = movie[args[p]];

      container.append(text);
    }

    document.body.append(container)
  }); 
}

function loadCharacters (arr, prop) {
  arr.forEach((movie) => {
    let characters = getMovieCharacters(movie[prop], `.${movie.title.toLowerCase().split(' ').join('-')}`);
  }); 
}

function getMovieCharacters (queries, parentClass) {
  const result = [];

  for (let i = 0; i < queries.length; i++) {
    const xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState > -1) {
        statusBar(this.readyState, parentClass);
      }

      if (this.readyState == 4 && this.status == 200) {
        result.push(JSON.parse(this.responseText));
      }

      if (this.readyState == 4 && this.status == 200 && i === queries.length - 1) {
        showCharacters(result, parentClass);
      }
    };
    xhttp.open("GET", queries[i], true);
    xhttp.send();
  }

  return result;
}

function showCharacters (arr, parentClass) {
  const parent = document.querySelector(parentClass);
  const title = document.createElement("H3");
  title.innerText = "Characters:";
  parent.append(title);

  arr.forEach(elem => {
    const text = document.createElement("P");
    text.innerText = elem.name;

    parent.append(text);
  });
}

function statusBar (status, parent) {
  const parentElem = document.querySelector(parent);
  let filling = parentElem.querySelectorAll(".status-filling")[0];

  if (!parentElem.querySelectorAll(".status-container")[0]) {
    const container = document.createElement("DIV");
    const fill = document.createElement("DIV");

    container.classList.add("status-container");
    fill.classList.add("status-filling");
    
    container.append(fill);
    parentElem.append(container);
    
    filling = parentElem.querySelectorAll(".status-filling")[0];
  }

  if (status == 0) {
    filling.style.width = "20%";
  }

  if (status == 1) {
    filling.style.width = "35%";
  }

  if (status == 2) {
    filling.style.width = "50%";
  }

  if (status == 3) {
    filling.style.width =  "75%";
  }

  if (status == 4) {
    filling.style.width = "100%";
    parentElem.querySelectorAll(".status-container")[0].style.visibility = "hidden";
  }
}