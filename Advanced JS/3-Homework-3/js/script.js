"use strict";

class Employee {
  constructor (name = "Ivan", age = 99, salary = 666) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name () {
    if (!this._name){
      return `${this.constructor.name}'s name is not set`;
    }

    return `${this.constructor.name}'s name is ${this._name}`;
  }

  get age () {
    if (!this._age){
      return `${this.constructor.name}'s age is not set`;
    }

    return `${this.constructor.name}'s age is ${this._age}`;
  }

  get salary () {
    if (!this._salary){
      return `${this.constructor.name}'s salary is not set`;
    }

    return `${this.constructor.name}'s salary is ${this._salary}`;
  }
  
  set name (name) {
    if (parseInt(name)) {
      alert(`Name shoould be a string`);
      return;
    }

    this._name = name;
  }

  set age (age) {
    const newAge = parseInt(age);

    if (isNaN(newAge)) {
      alert(`Age shoould be a number`);
      return;
    }

    this._age = newAge;
  }

  set salary (salary) {
    const newSalary = parseInt(salary);

    if (isNaN(newSalary)) {
      alert(`Salary shoould be a number`);
      return;
    }

    this._salary = newSalary;
  }
}

class Programmer extends Employee {
  constructor (lang = "en", name, age, salary) {
    super(name, age, salary);
    this._language = lang;
  }

  get language () {
    if (!this._language){
      return `${this.constructor.name}'s language is not set`;
    }

    return `${this.constructor.name}'s age is ${this._language}`;
  }

  get salary () {
    return `${this._salary * 3}`;
  }

  set language (lang) {
    if (parseInt(lang) || lang.length > 3) {
      alert(`Language should be a string with the lenght of 3 charachters`);
      return;
    }

    this._language = lang;
  }
}

const prog1 = new Programmer ("rus", "Ivan", 23, 1000);
const prog2 = new Programmer ("ukr", "Taras", 23, 1400);
const prog3 = new Programmer ("eng", "Bob", 26, 8000);

console.log(prog1)
console.log(prog1)
console.log(prog3)