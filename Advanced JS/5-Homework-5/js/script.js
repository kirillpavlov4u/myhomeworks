"use strict";

const requestButton = document.querySelector("button");

requestButton.addEventListener("click", () => {
    sendQuery("query");
});

function serverRequest (queries) {
  return new Promise ((resolve, reject) => {
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        resolve(this.responseXML);
      } else if (this.readyState == 4 && this.status > 400){
        reject("Some troubles appeared during request" + xhttp.response);
      }
    };
    xhttp.open("GET", `http://ip-api.com/xml/?fields=${queries}`, true);
    xhttp.send();
  });
}

function parseXML (XMLdata, elements) {
  let ID = "";

  elements.forEach(query => {
    const text = document.createElement('P');

    if (query === "query") {
      ID = XMLdata.getElementsByTagName(query)[1].textContent;
      return;
    }

    if(query.match(/\d+/g) !== null) {
      return;
    }

    if (XMLdata.getElementsByTagName(query)[0].textContent == "") {
      text.textContent = `${query} : !No information on this subject was found`;
      document.body.append(text);
      return;
    }

    text.textContent = `${query} : ` + XMLdata.getElementsByTagName(query)[0].textContent;
    document.body.append(text)
  });

  if (ID) {
    sendQuery(ID, "continent", "country", "regionName", "city", "district");
  }
}

async function sendQuery (...queries) {
  const promise = serverRequest(queries);
  let parse = await promise.then(value => { parseXML(value, queries) });
}