"use strict";

const books = [{
  name: 'Harry Potter',
  author: 'J.K. Rowling'
}, {
  name: 'Lord of the rings',
  author: 'J.R.R. Tolkien'
}, {
  name: 'The witcher',
  author: 'Andrzej Sapkowski'
}];

const bookToAdd = {
  name: 'Game of thrones',
  author: 'George R. R. Martin'
}

// Solution 1
const fullLibrary1 = books.concat(bookToAdd);

// Solution 2
const fullLibrary2 = [...books, bookToAdd];

console.log(`Task-5:`);
console.log(fullLibrary1);
console.log("-------------------------------------------------------")