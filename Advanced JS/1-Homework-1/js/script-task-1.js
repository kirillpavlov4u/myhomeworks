"use strict";

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

// Solution 1
const combinedClientBase = [...clients1, ...clients2];

function mergeArrays1 (arr) {
  return [...new Set(arr)];
}

// Solution 2
function mergeArrays2 () {
  const args = [...arguments].flat(); 
  
  return [...new Set(args)];
}

// Solution 3
function mergeArrays3 () {
  let result = [];
  const args = [...arguments].flat(); 

  args.forEach(data => {
    if (!result.includes(data))
    result.push(data);
  });
  
  return result;
}

console.log(`Task-1: `);
console.log(mergeArrays2(clients1, clients2));
console.log("-------------------------------------------------------")