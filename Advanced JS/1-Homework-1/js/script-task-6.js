"use strict";

const employee = {
  name: 'Vitalii',
  surname: 'Klichko'
}

const modifiedEmployee = {...employee, age: 20, salary: "4K"}

console.log(`Task-6:`);
console.log(modifiedEmployee);
console.log("-------------------------------------------------------")