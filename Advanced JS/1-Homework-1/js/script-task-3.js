"use strict";

const user1 = {
  name: "John",
  years: 30
};

const {name, years:age, isAdmin = false} = user1;

console.log(`Task-3:`);
console.log(`${name}, ${age}, ${isAdmin}`);
document.body.insertAdjacentHTML('beforeend', `${name}, ${age}, ${isAdmin}`);
console.log("-------------------------------------------------------")