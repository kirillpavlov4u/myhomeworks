export default class Post {
  constructor({userId, id, title, body}, users) {
    this.userId = userId;
    this.id = id;
    this.title = title;
    this.body = body;

    this.userInfo = {
      userData: users.filter(user => user.id === this.userId) || [],
      customUser: users,
      name: "",
      email: ""
    }

    this.elements = {
      container: document.querySelector(".news-feed"),
      postWrapper: document.createElement("DIV"),
      user: document.createElement("P"),
      userEmail: document.createElement("P"),
      title: document.createElement("H4"),
      text: document.createElement("P"),
      btnEdit: document.createElement("BUTTON"),
      btnDelete: document.createElement("BUTTON")
    }

    this.editElements = {
      updateBtn: document.createElement("BUTTON"),
      discardBtn: document.createElement("BUTTON"),
      editArea: document.createElement("TEXTAREA")
    }
  }

  setUser() {
    if (this.userInfo.userData[0] === undefined) {
      this.userInfo.name = this.userInfo.customUser[0].name;
      this.userInfo.email = this.userInfo.customUser[0].email;
    } else {
      this.userInfo.name = this.userInfo.userData[0].name;
      this.userInfo.email = this.userInfo.userData[0].email;
    }    
  }

  addClasses() {
    this.elements.postWrapper.classList.add("post-wrapper"),
    this.elements.user.classList.add("post-wrapper__user"),
    this.elements.userEmail.classList.add("post-wrapper__email"),
    this.elements.title.classList.add("post-wrapper__title"),
    this.elements.text.classList.add("post-wrapper__text"),
    this.elements.btnEdit.classList.add("edit-button"),
    this.elements.btnDelete.classList.add("delete-button")
  }

  addContent() {
    this.elements.title.textContent = this.title;
    this.elements.text.textContent = this.body;
    this.elements.user.textContent = this.userInfo.name;
    this.elements.userEmail.textContent = this.userInfo.email;
    this.elements.btnEdit.innerHTML = "<img src='https://image.flaticon.com/icons/png/512/61/61456.png' alt='edit-button'></img>";
    this.elements.btnDelete.innerHTML = "<img src='https://image.flaticon.com/icons/png/512/70/70757.png' alt='delete-button'></img>";
    this.elements.postWrapper.append(
      this.elements.title,
      this.elements.user,
      this.elements.userEmail, 
      this.elements.text,
      this.elements.btnEdit,
      this.elements.btnDelete
    )
  }

  setListeners () {
    this.elements.btnEdit.addEventListener('click', (event) => {
      this.editPost();
    });
    this.elements.btnDelete.addEventListener('click', (event) => {
      this.deletePost();
    });
  }

  toggleVisibility () {
    if (this.elements.btnEdit.hidden == true) {
      this.elements.btnEdit.hidden = false;
      this.elements.btnDelete.hidden = false;
      this.elements.text.hidden = false;

      this.editElements.updateBtn.hidden = true;
      this.editElements.editArea.hidden = true;
      this.editElements.discardBtn.hidden = true;
    } else {
      this.elements.btnEdit.hidden = true;
      this.elements.btnDelete.hidden = true;
      this.elements.text.hidden = true;

      this.editElements.updateBtn.hidden = false;
      this.editElements.editArea.hidden = false;
      this.editElements.discardBtn.hidden = false;
    }
  }

  setEditInterface() {
    this.toggleVisibility();

    this.editElements.updateBtn.textContent = "Apply changes";
    this.editElements.discardBtn.textContent = "Discard";
    this.editElements.editArea.textContent = this.body;
    this.elements.postWrapper.append(
      this.editElements.editArea,
      this.editElements.updateBtn,
      this.editElements.discardBtn
    );
    
    this.setEditInterfaceListeners();
  }

  setEditInterfaceListeners() {
    this.editElements.updateBtn.addEventListener("click", () => {
      event.stopImmediatePropagation();
      const textAreaValue =  this.editElements.editArea.value;
      this.body = textAreaValue;
      this.elements.text.textContent = this.body;
      
      this.putRequest();
    });

    this.editElements.discardBtn.addEventListener("click", () => {
      event.stopImmediatePropagation();
      this.toggleVisibility();
    });
  }

  editPost() {
    this.setEditInterface();
    this.setEditInterfaceListeners();
  }

  async deletePost() {
    let userConfirm = confirm("Delete this Twit?");
    if (userConfirm) {
      await fetch(`https://jsonplaceholder.typicode.com/posts/${this.id}`, {
        method: 'DELETE'
      });

      this.elements.postWrapper.hidden = true;
    } else {
      return;
    }
  }

  render() {
    this.setUser();
    this.addClasses();
    this.setListeners();
    this.addContent();

    this.elements.container.prepend(this.elements.postWrapper);
  }

  async putRequest() {
    await fetch(`https://jsonplaceholder.typicode.com/posts/${this.id}`, {
    method: 'PUT',
    body: JSON.stringify({
      id: this.id,
      title: this.title,
      body: this.body,
      userId: this.userId
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })

  this.toggleVisibility();
  } 
}