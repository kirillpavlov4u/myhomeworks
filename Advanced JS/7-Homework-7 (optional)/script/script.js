"use strict";

import Post from "./postClass.js";

document.addEventListener("DOMContentLoaded", () => {
  getPosts();
  createNewTwitListener();
});

async function getPosts() {
  const urls = ["https://jsonplaceholder.typicode.com/users", "https://jsonplaceholder.typicode.com/posts"];
  const loader = document.querySelector(".loader");

  await Promise.all(urls.map(u => fetch(u)))
  .then(
    responses => Promise.all(
      responses.map(res => res.json())
    )
  )
  .then(data => { data[1].forEach(
       post => new Post (post, data[0]).render()
      )
    }
  )

  loader.hidden = true;
}

function createNewTwitListener () {
  const addNewTwit = document.querySelector("#addPost");
  addNewTwit.addEventListener("click", () => createNewTwit());
}

function createNewTwit () {
  const form = document.forms[0];
  
  if (form.hidden) {
    form.hidden = false;
  } else {
    form.hidden = true;
    return;
  }

  const submit = document.querySelector("#submitNewTwit");
  submit.addEventListener('click', onSubmit);
}

function onSubmit (event) {
  event.preventDefault();

  const form = document.forms[0].elements;
  const postsCounter = document.querySelectorAll(".post-wrapper").length;

  const newPostData = {
    id: postsCounter + 1,
    userId: 1,
    title: form.title.value,
    body: form.body.value
  }

  const userData = [
    {
    name: form.name.value,
    email: form.email.value
    }
  ]

  if (form.body.value === "") {
    alert("Enter twit text!")
  } else {
    new Post(newPostData, userData).render();
    document.forms[0].hidden = true;
  }
}