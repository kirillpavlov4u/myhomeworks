"use strict";

document.addEventListener("DOMContentLoaded", () => {
  checkLibraryState();
});

const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
];

function showLibrary (status) {
  let library = [];

  if (status) {
    library = checkLibrary(books, true, "author", "name", "price");
    return library;
  } else {
    library = checkLibrary(books, false, "author", "name", "price");
  }
}

function checkLibrary (library, status, ...props) {
  let result = [[], []];

  library.forEach((book) => {
    let allProps = true;

    Array.prototype.forEach.apply(props, [(prop) => {
      if (book[prop] === undefined) {
        allProps = false;
      }
    }]);

    if (allProps) {
      result[0].push(book);
    } else {
      result[1].push(book);
    }
  });

  if (status === true) {
    return result[0];
  } else {
    if (result[1].length) {
      throw {message: "Books with not a full refference found: ", books: result[1]}
    }
  }
}

function displayLibrary (library) {
  const container = document.getElementById("root");
  const fragment = document.createDocumentFragment();
  const list = document.createElement("UL");
  
  list.classList.add("list");
  

  library.forEach(book => {
    const listItem = document.createElement("LI");
    listItem.classList.add("book");

    listItem.innerHTML = `<h3>${book.author}</h3><i>"${book.name}"</i><b> Price: $${book.price}</b>`;

    fragment.append(listItem);
  });

  list.append(fragment);
  container.append(list);
}

function checkLibraryState () {
  try {
    showLibrary(false);
  } catch (err) {
    console.error(err.message, err.books)
  } finally {
    displayLibrary(showLibrary(true))
  }
}

