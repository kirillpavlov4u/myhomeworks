"use strict";

let gameInterval;
let gameTimeout;

const startButton = document.getElementById('startGme');
startButton.addEventListener("click", startTheGame);

function startTheGame (event) {
  event.preventDefault();
  const winnerWindow = document.querySelector(".initiate-game");
  const level = document.forms[0].elements.gameLevel.value;
  level.toLowerCase();

  if (level === "easy") {
    new Game().startGame("Easy");
  }

  if (level === "medium") {
    new Game().startGame("Medium");
  }

  if (level === "hard") {
    new Game().startGame("Hard");
  }

  winnerWindow.style.display = "none";
}

class Game {
  debugger;
  constructor() {
    this.userCounter = document.querySelector(".user-win-counter");
    this.blueCounter = document.querySelector(".cell-counter");
    this.computerCounter = document.querySelector(".computer-win-counter");
    this.gameContainer = document.querySelector(".the-game");
    this.cellTable = new Table();
    this.winnerWindow = new WinnerWindow(this.gameContainer);
    this.emptyCells = document.getElementsByClassName("empty");
    this.userWinCounter = 0;
    this.compWinCounter = 0;
    this.cellCounter = 0;
    this.refreshTime;
  }

  assignColor (cell, color, thisRefference) {
    if (color === "red") {
      cell.classList.replace("blue", color);
      thisRefference.compWinCounter += 1;
      thisRefference.updateScore();
      thisRefference.referee(color);
      return;
    }

    cell.classList.replace("blue", color);
    this.referee(color);
    clearTimeout(gameTimeout);
  }

  incrementUserScore (trigger) { 
    if (trigger.target.tagName === "TD")
    if (trigger.target.classList.contains("blue")) {
      this.userWinCounter += 1;
      this.updateScore();
      this.assignColor(trigger.target, "green");
    }
  }

  referee (color) {
    if (document.getElementsByClassName(color).length === document.getElementsByTagName("TD").length / 2) {
      if (color === "red")  {
        this.endGame();
        this.cellCounter -= 1;
        this.updateScore();
        document.getElementsByClassName("blue")[0].classList.replace("blue", "empty");
      } else {
        this.endGame();
      }
    }
  }

  updateScore () {
    this.userCounter.innerText = this.userWinCounter;
    this.computerCounter.innerText = this.compWinCounter;
    this.blueCounter.innerText = this.cellCounter;
  }

  blueCellSpawner (thisRefference) {
    const index = Math.floor(Math.random() * (thisRefference.emptyCells.length));

    const blueCell = thisRefference.emptyCells[index];
    blueCell.classList.replace("empty", "blue");

    thisRefference.cellCounter += 1;
    thisRefference.updateScore();

    gameTimeout = setTimeout(thisRefference.assignColor, thisRefference.refreshTime, blueCell, "red", thisRefference)
  }

  setGameDifficultLevel (speed) {
    if (speed === 'Easy') {
      this.refreshTime = 1500;
    } else if (speed === 'Medium') {
      this.refreshTime = 1000;
    } else if (speed === 'Hard') {
      this.refreshTime = 650;
    }
  }

  startGame (difficultLevel) {
    const thisRefference = this;

    this.gameContainer.style.display = "block";
    
    this.userWinCounter = 0;
    this.compWinCounter = 0;
    this.cellCounter = 0;
    this.updateScore();

    this.cellTable.newTable();

    this.gameContainer.addEventListener("click", (event) => {
      if (event.target.tagName === "TD" && event.target.classList.contains("blue")) {
        this.incrementUserScore(event);
        this.referee("green");
      }
    });

    this.setGameDifficultLevel(difficultLevel);

    gameInterval = setInterval(thisRefference.blueCellSpawner, thisRefference.refreshTime, thisRefference);
  }

  endGame () {
    clearInterval(gameInterval);

    while (gameTimeout) {
      window.clearTimeout(gameTimeout);
      gameTimeout--;
    }

    this.winnerWindow.chooseTheWinner(this.userWinCounter, this.compWinCounter);
  }
}

class Table {
  constructor () {
    this.container = { 
      tableContainer : document.querySelector(".playground-container")
    };
    this.elements = {
      table : document.createElement("TABLE")
    }
  }

  newTable () {
    this.container.tableContainer.innerHTML = '';

    for (let i = 0; i < 10; i++) {
      const newRow = this.elements.table.insertRow();
      for (let k = 0; k < 10; k++) {
        newRow.insertCell();
      }      
    }

    this.container.tableContainer.append(this.elements.table);

    this.addEmptyClass();
  }

  addEmptyClass () {
    const cells = document.getElementsByTagName("TD");

    Array.prototype.forEach.call(cells, (cell) => {
      cell.classList.add("empty");
    });
  }
}

class WinnerWindow {
  constructor (gameContainer) {
    this.winnerWindow = document.querySelector(".initiate-game");
    this.winnerText = document.getElementById('winner');
    this.gameContainer = gameContainer;
  }

  chooseTheWinner (userScore, compScore) {
    this.winnerWindow.style.display = "flex";
    this.winnerText.hidden = false;
    this.gameContainer.style.display = "none";

    if (userScore > compScore) {
      document.getElementById('winner').innerText = `The winner is: Player.\nScore:\nPlayer: ${userScore}; Machine: ${compScore};`;
      return;
    } else if (userScore < compScore) {
      document.getElementById('winner').innerText = `The winner is: Machine.\nScore:\nMachine: ${compScore}; Player: ${userScore};`;
      return;
    }

    document.getElementById('winner').innerText = "Draw!";
  }
}