|  ***My theoretical Q&A*** |
| :---: |

# Basic HTML and CSS

## 1.1 Intro Front-End

>   1.What is Front-end development?

Front-end is part of a virtual product with which user can directly <br />
interract: design and in-browser/device functionality.<br />

>    2.What Front-end is responsible for?

It is responsible for UX(functionality/use interaction) <br />
and UI(feeling of your project(web page, app or whatever)).<br />

>    3.Where does Front-end code gets executed?

In the user's browser on PC or portable electronics.

>   4.What aspects of web page are done with HTML, CSS and JavaScript?

HTML - is for adding content;<br />
CSS - for styling and adding animations and some basic level functionality.<br />
JS - adds functionality, allows to dynamicaly manipulate content of the<br />
page and give ability to work with te server side.<br />

>  5.Which HTML tags do you know?

Those which need closing tags and that which does not. For example ```<link>``` <br />
and ```<script></script>``` tags.<br />

>   6.How to create a link in HTML?

Simply create ```<a>``` element, add href property, add title and you should add<br />
url text inside ```<a>``` tag.<br />

>   7.How do you include an image in HTML page?

Simply create ```<img>``` element, specify src attribute and alt description of an<br />
image, in case the image doesn't upload to your page.<br />


## 2.1 CSS basics

>  1.What is purpose of Emmet?

Emmet's plugin purpose is to accelarate development process with the help of<br />
basic programmed shorthand commands.<br />

>   2.How can we include CSS styles on HTML page?

Sure, this is calle inline styling and it's bad practise.

>   3.What is the difference between margin and padding?

Margin - it is outter distance between to elements, whilst<br />
Padding - is a distance between border of a element and its content.<br />

>   4.Can margin value be negative?

Sure it may be, but it's nice tone to avoid such practice.

>  5.How do we write comment in CSS and why do we need them?

We add comments inside css file by adding ```/* <your comment goes here> */.```

>   6.How can we set a background color of the HTML element?

By using background-color css property.

>   7.How can we set a background image of the HTML element?

By using either background or background-image css property.

> 8.Can we have both background color and background image?

Sure.

> 9.Can we change styling of the web page without changing HTML layout?

Absolutely.

> 10.Can web page work without CSS or JavaScript?

Technically speaking - sure, no problem. But it will be a poor job.

## 3.1 "Working with Git Console"

> 1) What is Git?

Git - a repository that stores your project's files. With git you able to<br />
store your project in the git's cloud and your personal computer or remote <br />
server.<br />
Also, with Git you can easyly adjust your work with team of developers by<br />
adding them to a project where you may add changes simultaneously.<br />

> 2) How do you create a new Git repo.?

You may initialize new repo. via Git web interface and than ```$ git``` clone to <br />
your remote server or PC. Or you may create a new folder on your computer<br />
and than `$ git init` to the Git's Cloud.<br />

> 3)How do you commit changes to repository?

First, you must ```$ git add .```new data to make them visible for Git's<br />
system.<br />
Second, it's a good practice to``` $ git commit -m "<Your description>"``` every <br />
new changes to repository.<br />
Third, you ```$ git push``` data to Git's Cloud.<br />
P.S.: if it is your first commit, you should set your <br />
```$ git config --global user.name <"Your full name here">``` and <br />
```$ git config --global user.email <"Your e-mail goes here">```.<br />

> 4)How can you check status of the changes made to repository?

You just input into console ```$ git status``` or ```$ git log``` and you can see <br /> 
the history of changes to repository or current status of new/updated/edited files.<br />

> 5)What is a branch?

Branch is a container of directories which contains project/s. You may also <br /> 
refer to a Branch as a version of a project (if you run such type of <br />
development strategy). <br />


> 6)How do we work with Git in teams?

Members of a team, who are authorized to work with a project on specified<br />
terms should have remoute copy of a project on theirs machines.<br />
Every new `git push` should be previously `$ git commit -m "<Your descrription>`<br />
`of a version here">`. Every changes/versions of/to a project should be trackable.<br />
Every member of project's team who are authorized to `$ git push` new data to<br />
a repository, should set their names and email address so that every changes<br />
could be bounded to responsible for this changes person.

## 4.1 "What is CSS and how it works"
> 1) Why do we need reset.css?

We need reset.css stylesheet to drop all browser's style setting<br />
to  default state.

> 2) What is a block model?

It is a structure of a block element in css which consists of:<br />
a) content box,<br />
b) padings,<br />
c) borders and<br />
d) margins.

> 3) How does inline, block and inline-block modes work?

Inline model - is a state of an element when it is placed in one row with <br />
other elements or content and height and width has no effect.<br />
Inline-box model - is a statte of an element when it is place with other <br />
element in a single row, but it's height and width settings could be <br />
adjusted.<br />
Block model - is a model of an element which places a specific element<br />
into its single row. Width and height could be adjusted too.

> 4) What is the purpose of box-sizing property?

This property allows to define which settings of padding, margin and<br />
borders to include into calculation of a box size.

> 5) How can you examine HTML elements in your browser?

Through developers console in a browser.

## 5.1 CSS selector.
> 1) What is CSS selector? 

A CSS selector is a key or a pattern of keys  which selects needed elements<br />
on a web page which should be styled.

> 2)Which CSS selectors do you know?

There are:
A universal selector `* {} (selects all elements on a web page)`,<br />
Type selectors `a/p/html {} (selects specified elements on a whole web page)`,<br />
Class selectors `.navbar {} (selects an element with by the class)`,<br />
Id selectors `#navbar-block {} (selects an element by the unique ID)`,<br />
Group selectors `a, p, html {} (selects all given elements)`,<br />
Decendant selector `div a {} (selects all elements which are placed on the`<br />
`level lower then parent's through all web page)`, <br />
Child selector `div > a {} (selects child elements of a specified parent)`,<br />
Adjusted sibling selector `div + a {} (selects first a element that goes right`<br />
`after div)`,<br />
Adjusted sibling selector `div ~ a {} (selects all a elements that go right`<br />
`after div)`,<br />
Attribute selector `input[type="text"] {} (selects an element with specified` <br />
`attribute)`;

> 3) Which attribute has higher weight: “class” or “id”?

"ID" attribute has higher priority than "class" atribute;

> 4)What is the weight of the following selector? h2#page-header -<br />
a.logo-text.text-white ?

111 - 1 for element, 10 for class, 100 for ID.

> 5) How to overwrite inline styles placed in HTML using CSS?

You may add a "!important" keyword after every value for each attribute;

> 6)Name some absolute and relative css units.

cm, mm, px;

> 7)When do we use absolute units and when relative?

We use absolute units, when we know for shure what screen size our<br />
user is using right now. We may utilize this trick by usic `@media` queries<br />
and give rules under which or web page will be adjusted to fit specified <br />
screen sizes.<br />
We use relative css unit to ease our web page styling when we have a goal<br />
to make it responsive.

> 8)What is the difference between em and rem css units?

Em - relative to size of container's font size.<br />
Rem - relative to the size of thу root's elemtn font size;

> 9)What is the difference between % and vh/vw units?

% - relative to the parent element;
vw/vh - relative to the vieport (browser's screen) size;

## 6.1 Floating elements.

> 1) What is the default value of “display” property

for elements with `“float: right”`?
The default `"display"` property for float elements is `"block"`.

> 2)How floating elements affect height of parent element?

A float element in no way will affect its parent element. For parent<br />
element, if you didn't declared width and heigth of a parent, will<br />
behave itself as if it's empty.

> 3)How elements going after tags with “float” property are displayed?

They will try to take place to the left of the page, to the right or<br />
will inherit parents settings of float property.

> 4)How does `“clear: right”` work?

A `"Clear: right;"` property will prohibit elements to float this element on<br />
its right side.

> 5)Which element has to contain “clear: both” if you want to fix the problem 
with overlapping floating elements?

A `“clear: both”` property should have only those elements which shold occupy<br />
a whole row.

> 6)What fonts do we call “web safe”?

Web-safe font are those which are built in you browser or computer or other <br />
device.

> 7)What is the difference between serif and sans-serif fonts?

Serif gives text much grotesque fill with letters end strokes while<br />
`sans-serif` is more conservative font style type.

> 8)How can you add custom font on a page?

You may:<br />
a) you may use a custom font by defining it in css,<br />
b) or you may import external font into html or css files.

## 7.1 Pseudo Elements CSS.

> 1)What is the difference between pseudo-classes and pseudo-elements?

Pseudo-classes are classes which are more like condition which are call<br />
certain styles under sertain pseudo-class.<br />
Pseudo-elemets are elements which are created in stylesheets - CSS file.

> 2)To which tags can you apply the :hover pseudo-class?

Any element of your DOM structure may accept `:hover` ps-class.

> 3)How to change css-rules for a 4-th element with the same class?

you should specify a .class of the element and then add ps-class <br />
`:nth-child(4)`.

> 4)How does the “:not” pseudo-class work?

it selects all elements except that which are specified in `:not()` <br />
ps-class.

> 5)What CSS property is required when you use pseudo-elements?

if you want to create an element, it's necessary to use `"content"`<br />
property.

> 6)What is the default value of “display” property for pseudo-elements
`::before` and `::after`?

The deault value is `"inline"`;

> 7)How to apply a style for a first letter in a text block?

You may first select an element? which contains such a text, and te add<br />
a pseudo-element `::first-letter`.

## 8.1 Colors, Opacity, Psition.

> 1)What positioning shall you use to create a footer attached to bottom of the page?

Sticky, Static, Fixed


> 2)Will absolutely positioned `<div>` push away other absolutely positioned elements?

Nope.

> 3)What are the two use cases for `“position: relative”`?

When you need to position element relatively to the place where it is<br />
situated, or when you need to assign position: absolute to its child.

> 4)Will absolutely positioned `<div>` be always shown on top of other elements?

It will be show under relatively positioned elemts unless you are using<br />
`z-index` property to place absolutely positioned element above other.

> 5)What CSS property defines which of the layers will be overlapped?

`Z-index`.

> 6)Which ways of assigning a color in CSS do you know?

5 ways. `HEX`, `RGB`, `RGBa`, `HSL`, `HSLa`.

> 7)What are the ways of making block’s background transparent?

By using `background-color` property with `RGB` or `HSLa` values.

> 8)What is the difference between `“opacity”` and `“background-color”` with transparency?

Opacity property set the opacity level of an element whilst background-color <br />
property sets the color of background of an elements where you also may set <br />
opacity level for a color.

## 9.1 Styling inputs.

> 1.Why do we use sprites instead of many small images?

To decrease page load time.

> 2.What is the required <form> attribute for a request to be correctly sent?

These attributes are method and action.

> 3.What is the default “display” property of form inputs?

It is an inline-block.

> 4.Can we style a checkbox?

Sure. But you can not style default html checkbox. You should hide the <br />
default and create your own object.

## 10.1 Tables.

> 1.Explain the HTML Table structure and name its elements.

Any table consists of rows and data cells (td) in each row. <br />
Here are some of table's elements: <br />
table, table head (thead), table main data (tbody), table footer (tfoot), <br />
table row (tr), table header (th), table data or cells (td), also you may <br />
add table captioning (caption).

> 2.Name the HTML5 tags that we could use to describe page layout.

This elements are: `header, nav, section, main, aside, footer`.

> 3.How can we create a gradient background with CSS3?

By using `"background"` property.

> 4.What types of gradient backgrounds are there?

There are linear and radial gradients.

> 5.How can we create a shadow for a text and for a box-element?

For text we could use `text-shadow`, and for an element - `box-shadow`.

> 6.Name some transform methods and their purpose.

`translate()` - property which moves an element on `X` and `Y` axis. <br />
`rotate()` - property which rotates an object clockwise and counterclockwise. <br />
`scale()` - enlarges an element's width and heigth. <br />
`kewX()` and `skewY()` - movement similar to to a rotation but which appears on <br />
`X` and `Y` axis. <br />
`matrix()` - combines all rotation methods into one. On my opinion is too <br />
complicated.

> 7.Can we calculate CSS values dynamically? How?

Sure, by using `calc()` css method.

## 11.1 FlexBox

> 1.What is flexbox?

It is a variant of displaing an container. It makes everything insid respond <br />
to view port's width and height changes.

> 2.Why should we use flexbox?

To create responsive and flexible designs.

> 3.How can we align items inside a flex container?

With: <br />
`align-items` property, <br />
`flex-direction` property, <br />
`flex-wrap` property, <br />
`flex-flow` property, <br />
`justify-content` property, <br />
`align-items` property, <br />
`align-self`.

> 4.How does “justify-content: space-around” work?

It sets rules for horizontal aligning of flex elements and space between <br />
them.

> 5.What is the difference between “align-items” and “align-content”?

align-items works with elements inside flexible row while<br />
align-content works wit aligning flexible rows.

> 6.How to position a flex-element in the end of row?

By using `justify-content: flex-end`;

> 7.How can you create a flex-element that will increase its width?

By adding extra property - `flex-grow`.

> 8.How to vertically align only one flex-element?

by utilizing `align-self` property.

## 12.1 The Grid.

> 1.What is grid layout?

The grid layout is one of methods to position elements/ blocks on the <br />
webPage.

> 2.What does grid layout makes easier?

With Grid layout you do not need to specify floats and extensive positioning <br />
parameters.

> 3.What is the difference between the flexbox and grid approaches?

Flexbox doesn't aloow you to work directly with rows and columns in one <br />
flexbox template. Whilst Gridbox automaticaly give you an option to set <br />
elements positioning om certain rows and columns.

> 4.When will you use flexbox and when grid layouts?

Grid is a block oriented positioning and sizing tehnique whilst flexbox is <br />
content oriented sizing and positioning technique.

## 13.1 Animation

> 1.For which 4 properties is transition a shorthand?

a)property b)duration c)transition-timing-function d)delay;

> 2.How to add transition effect to all changed CSS properties?

You may includde them into 1 transition property separating them with comma.

> 3.How does cubic-bezier value for transition-timing-function work?

It allows you to specify custom timing function for your animation.

> 4.What is the default value for animation-iteration-count?

The default value is "1".

> 5.How does animation-fill-mode:backwards work?

Element's animation will start with style values from `@keyframe` "from" <br />
stage.

> 6.How will the following animation work?

0%   {motion-offset:0;} <br />
100% {motion-offset:100%;} <br />


> 7.What is parallax’s effect?

It is a styling technique in which background moves slover than webPage <br />
during scrolling.

> 8.How to create simple parallax effect with pure CSS?

You will need a container and a background image. Then use property <br />
background attachement and use some positioning properties.

> 9.How does “perspective” CSS property work?

It specifies how far an element is positioned away from the user on the <br />
webpage

> 10.How to change depth of parallax effect?

By adding multiple background layers and asigning different scrolling speed.

> 11.How to change speed of parallax effect during scrolling?

by using perspective and `translateZ` properties.

# JS

## 1. JS Basics.

> 1.What is JavaScript and where is it used?

JavaScript is a programming language which allows to perform differnt<br />
actions with WebPages and it's content.<br />
You also may perform actions with server side (Node.js) and a lot more.

> 2.How can we add JavaScript to an HTML page?

You may add JS by adding `<script></script>` tags inside `html` document<br />
inside `<head></head>` tags, or before `<body></body>` tags or by adding<br />
separate .js file with the code inside it.

> 3.What is the difference between let and var variables?

`let` is a block scope variable, whilst `var` is a function scope. basically,<br />
it means with `let` variables you have quite strict rules for accessing<br />
data which is situateв on levels higher than current scope <br />
(function scope, block scope, global scope etc.)

> 4.What is the naming convention for variables in Javascript?

For `let` and `var` variables you should use `camelCase` naming rules.<br />
For `const` you should use `UPPERCASE` and `_` when you need to add space<br />
between words in the naming

> 5.What is a constant?

`const` is a type of variables (in ES6) which could not be changed or<br />
modified after it was declared.

> 6.Why do we need strict mode?

When you need to have more clear, corresponding with JS programming rules code.

> 7.Name the three simple ways to enter data in a browser and access it in JavaScript.

`alert()`, `romt()`, `confirm()` interraction windows and, in some way, <br />
a developers console.

## 2. JS Data Types

> 1.How many data types are there in Javascript?

There are 7 data types in JS.

> 2.Name all JavaScript basic types

1. Number
2. String
3. Boolean
4. Object
5. Symbol
6. null
7. Undefined

> 3.What type shall we use to store a text?

A `String` value type.

> 4.What type do we use for calculations?

`Number` (also, knowing that in JS in mathematical operations automatic <br />
type convertion exist, we may use in our calculation `strings`, `booleans`, <br />
`null`.)

> 5.What is the difference between undefined and null?

The value is `Undefined` - means the argument/variable is declared, but <br />
has no assigned value to it.
The `null` output - we recieve it when value of a variable doesn't belong <br />
to any JS value type or even variable/ argument, when it sould be, doesn't exists.

> 6.How can we cast one type to another?

By converting one using `String()` , `Number()` or `Boolean()` functions. <br />
Or, regarding mathematical operations, using math operators.

> 7.How can we check which data type does a variable store?

by using `typeof` / `typeof()` function.

## 3. JS Operators

> 1.What is an operator and what is an operand?

`Operator` - this is what performs actions, `Operand` - on whom actions<br />
are performed.

> 2.What is the difference between concatenation and addition?

`Concatenation` merges `strings` while `addition` sums up `values`.

> 3.How comparison of different data types works?

This can happen only with `equality operator` `==`. This phenomenon is called<br />
`Type Conversion` and it means you can evaluate values of different data types.<br />
It is also called `Type Coercion`.

> 4.How does if-else construction work?

First we set rules for `if` block and than program its output.<br />
Second (optional), for `else` block, we don't have to set rules, but <br />
should program output of this block , because it will be trigered only <br />
if first `if` block will return `false`.

> 5.How does switch operator work?

First, you set a value or expresion to the `switch` block (). <br />
Than, you create `cases` for evaluation and performing action.<br />
The `case` should end with `break;` command. <br />
You may also group several `cases` into one.<br />
Basically, every `case` is equivalent to `if () || else if ()` statements. <br />
And `default` is equal to `else`.

## 4. JS Loops

> 1.In which cases do we use loops?

When we need to do some repeated action on some data or DOM elements, arrays<br />
object's values, work with big data etc.

> 2.What is the difference between “while” and “do while” loops?

`While loop` checks if condition is `truthy` at its start whilst `do while loop`<br />
does it at the end of the first iteration.

> 3.How does the “for” loop work?

`for loop` works while given condition is `truthy` - basically it is used for <br />
counting. `for loop` condition consists of three parts: `begin`, `condition`, <br />
`step`.

> 4.How to skip a code block execution to the next iteration?

By using `continue` keyword.

> 5.How to break a cycle in the code block?

By using `break` keyword.

## 5-6 JS. Function

> 1.What is a function?

`Function` is an imprtant code block of an code document. it allows us to <br />
repeat the same action/computations again and again without doubling our code.<br />
`Declared functions` could be called from any part of our code document in any <br />
time (because `functions declarations` are processed by machine at first priority).

> 2.What are the ways to declare a function in Javascript?

There are two ways to declare a function: by using `function declaration` or <br />
`function expression`.

> 3.What is a variable scope?

This term is used to describe a phenomenon by which some variable could be <br />
accesed `outside a code block` or `only inside a code block`. 

> 4.What is a global scope?

`Global scope` is a root of a code document. It means that variables declared <br />
inside `global scope` could be accessed from any scope dimension (functions, <br />
logical code blocks, inside conditions, loops and other code blocks).

> 5.How type of variable influences its scope?

A `var` type variable is function scoped, but if it is declared inside other code <br />
block (`if..else` statement) it could be accessed outside this block, whilst 
`let` and `const` could not be.

> 6.What is a callback?

A `callback function` is type of a function's argument which is a function.

> 7.What is an arrow function?

A shorter version of a `function expression` or a shorder way to declare a function.

> 8.How can you debug your code?

By using `developer's tools` in our browser.

## 7 JS. Object

> 1.What is Object Oriented Programming?

An `OOP` is a programming paradigm which represents its basic building blocks <br />
as an `objects`.

> 2.What is an Object?

An `object` is an programming language entity which constitutes of `properties`<br />
anв `methods`.

> 3.How can you create an object in JavaScript?

By using `object constructor` or `object literal`; 

> 4.What is a method? How can we call one?

A `method` is an action which could be performed on a object.

> 5.Can an object have two methods with the same name?

No, it can not. Because the second function will overwride first declared function.

> 6.How can we clone an object?

by using object's method `Object.assign()` or `object function constructor`

> 7.How does the this keyword work?

The `this` keyword refers to parent object, whisch is a holder of `this` keyword.


## 8 JS. Strings, Date, Time

> 1.How can we add special character on a page?

Use backslash "\\" in a string and enter a unicode of a desired symbol aka <br />
special character.

> 2.How can we find a substring inside a string?

We may use this `String methods()`:
- `slice()`,
- `substring()`,
- `substr()`,
- `indexOf()`,
- `lastIndexOf()`,
- `includes()`,
- `startswith()`,
- `endsWith()`;


> 3.How is time and date stored in JavaScript?

Time in JS is represented by `Date Object`. It is stored in milliseconds, counting<br />
from 1st January 1970.

> 4.How can we find how much time has passed between two dates?

By finding their difference in millisecond in relation to 1st Jan 1970.

## 9 JS. Arrays

> 1.What is array?

`Array` is a stack of indexed data, where each index is stacked on previous one.

> 2.How can we create an array in Javascript?

By creating `new Array ()` function constructor or by opening `[]` square <br />
brackets.

> 3.What array methods do you know?

`shift()`, `unshift()`, `push()`, `pop()` etc.

> 4.What is the “rest” operator?

A `rest operator` creates an array from set of arguments.

> 5.What is the “spread” operator?

A `spread operator` creates a set of arguments out of an array.

> 6.How can we sort an array?

By using array `sort()` method and creating specific rule inside it to perform <br />
actual sorting.

> 7.How can we find an item in the array?

By usig `indexOf() / lastIndexOf()` methods.

> 8.What is an “iterable” in JavaScript?

`Iterables` - are objects that can be used in `for ..of` loop.
Strings, Arrays, Sets, Maps, Objects.

> 9.What is Set?

`Set` is a collection of unique values.

> 10.What is the difference between Array.push and Set.add?

`Array.push()` wil add any value to the array, while `Set.add` adds a value <br />
only if a similar value does not exist in a set.

> 11.What is Map?

`Map` - is a collection of "key: value" pairs just like an Object. But <br />
in `Map` key may be any value type. We can get a value by the key.

> 12.What can be used as a Map key?

In `Map` key may be any value type.

## JS. DOM

> 1.What is DOM?

`DOM` is the some kind of a soсket which allows progrmming languages to acess<br />
web-page elements and manipulate them.

> 2.What is a Parent, a Child and a Sibling in the DOM Tree?

All of this tree are objects. `Parent` is a object which may contain nested<br />
`Child` objects. `Siblings` are objects of the same parent.

> 3.What methods can you use to get a single DOM element?

`document.getElementsByID('element's ID goes here')`
`document.firstChild/lastChild`.

> 4.What methods can you use to get multiple DOM elements? 

`document.getElementsByClassName('element class name goes here')` or
`document.getElementByTagName('element tag name goes here')` or
`document.getElementByName('name attribute value goes here')` or 
`document.querySelectorAll('CSS selector goes here')`.

## JS. Working with DOM

> 1.What is an attribute?

Basically, in JS DOM representation it is a property which contains value or <br/>
an object which may content a list of properties.

> 2.What is the difference between class and className?

`Class` is reserved JS command while `className` is a property of an element in JS.

> 3.How can we move a node?

By using this commands:
`node.append()`
`node.prepend()`
`node.before()`
`node.after()`
`node.replaceWith()`

> 4.How can we remove a node?

By using this commands:
`parentElem.removeChild(node)`
`node.remove()`

> 5.What is the difference between clientX and pageX?

Window coordinates (`clientX, clientY`) which are measured relatively to <br/>
browser's window and document coordinates (`pageX, pageY`) which are measured <br/>
relatively to a document.

## JS. DOM Events

> 1.What is Event?

Event - is an action, which a user may perform while interracting with the <br/>
webpage.

> 2.How can we attach an event on the page?

You may set an `EventListener` or add an event tracker on html element as <br/>
attribute or as a property on a DOM element object. 

> 3.How can we detect which key was pressed?

By using `event.which`;

> 4.What event is triggered when page DOM is ready?

`DOMContentLoaded` or `onload`.

> 5.How can we set a select-element value?

`select.value ` or if we need to access selected option `select.selectedIndex`.

> 6.What event react to input-element changes?

An `input` event.

> 7.How can we process form-element on its submit?

By using `submit` event.

> 8.What is bubbling?

When event which is placed on parent element trigers event handler on a child <br />
elemnt first than on its parent.

> 9.How can we stop event propagation?

By specifying event method `event.stopPropagation()`;

> 10.When can we use event delegation?

When we have a need to assign many event trackers on many elements of one <br />
common ancestor.

## JS. Working With timers

> 1.How can we execute a delayed code?

By uset `setTimeout()` method.

> 2.What is the difference between setTimeout and setInterval?

The first executes code after some time have passed while `setinterval()` <br />
repeats code with specific time interval.

> 3.What should be avoided when working with timers?

`()` should be avoided. A timer method supposes to receive a function reference<br />
but not a function call.

> 4.What is localStorage?

An information related to a certain website which is stored on user's device.

> 5.What differs localStorage from cookies?

Can be stored infinitely and capable of storing more data.

## JS. jQuery

> 1.What is jQuery and where can you use it?

It is a JavaScript library. It's used in client side scripting.

> 2.How can you find DOM element using jQuery?

By any CSS selector.

> 3.Which jQuery methods do you know to manipulate DOM?

`$().on()`
`$().height()`
`$().after()`

> 4.How can we set CSS-styling with jQuery?

`$(<selector>).css(<property>, <value>)`
`$(<selector>).css({
    <property>: <value>,
    <property>: <value>
});`

But the best way to munipulate style is to create certain style specification <br />
inside the css file aтв change styles by changing element's classes or another<br />
attribute which triggers certain style preference.

> 5.How can we create new DOM-element with jQuery?

`let elem = $("<p>This is a new paragraph</p>");` - create an element and then <br />
`$("body").append($(elem));` - add it to the DOM.

> 6.How can we create an animation with jQuery?

By using standart jQuery anuimation methods or by using `animate()` method in <br />
which you may pass properties to change, duration of the animation and a callback.

> 7.How do jQuery plugins work?

jQuery plugins - are jQuery library extensions which are created on thу basis of <br />
jQuery library and its scripting conventions.

## Advanced HTML/CSS

## 1

## 2

## 3

## Adaptive/Responsive layout

> 1.Explain what is Pixel perfect development

Pixel perfect development - is an approach to reach the most<br />
precise programming of a web-page from design mockup.

> 2.Why is Pixel perfect important and when it could be used?

Each design is a certain architecture of a product? which is a web-page.<br />
One of many tasks of Front-end developer is to translate design to code.

> 3.What tools can we use to make Pixel perfect page?

You may use chrome's ЗукаусеЗшчуд extension to compare initial mockup <br />
to a final web-page.

> 4.Why is it important to adjust CSS code for blocks from <br />
top to bottom and from left to right during Pixel perfect development

To find initial "mark 0" point to set it as a starting point <br />
to which the mockup will be set and all elements will be <br />
relatively positioned.

> 5.What is the difference between adaptive and responsive approaches?

Adaptive design targets specific devices where final webInterface will be deployed.<br />
Responsive design targets no devices and creation of flexible design is its target - <br />
create such a design whic looks great on every device.

> 6.Why is such approaches are needed?

It may help you save you time on a development aтв may provide you <br />
with specific development rules/guidelines for you to stick to in order <br />
to create the best possible product.

> 7.How can we change page layout on mobile version?

By specifing certain rules incide `@media` query instruction.

> 8.What is viewport meta for?

A `vieport meta` is needed to adjust web-page scaling for <br />
different devices.

> 9.How can we test site for various target devices?

By using `taget device test` inside browsers `developer tools`.

> 10.What is media queries and how can we add them?

`@media`-queries are CSS instructions to adjust styles to a<br />
certain display setting or other parameters.
You may add them as an instruction in CSS file, as an attribute <br />
in `<meta>` tag.
