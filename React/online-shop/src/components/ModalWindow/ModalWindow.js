import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './ModalWindow.scss';
import { BsFillXSquareFill } from 'react-icons/bs';

class ModalWindow extends Component {
  render() {
    return (
      <div 
        className='modal-background'
        onClick={(event) => this.props.closeModal(event)}
      >
        <div className='modal-window'>
          <div className='modal-window__header'>
            <p className='modal-window__header__title'>Add product to the cart?</p>
            <BsFillXSquareFill className='modal-window__header__icon' id='close'/>
          </div>
          <div className='modal-window__body'>
            <p className='modal-window__body__text'>Are you shure you want to add this product to a cart?</p>
            <div className='modal-window__body__btn-wrapper'>
              <button
                id='addToCart'
                onClick={() => this.props.addToCart(this.props.productOnQueue)}
              >Add to cart</button>
              <button
                id='cancel'
              >Cancel</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default ModalWindow;

ModalWindow.propTypes = {
  closeModal: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  productOnQueue: PropTypes.string.isRequired
};