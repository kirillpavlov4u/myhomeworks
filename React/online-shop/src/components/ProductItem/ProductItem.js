import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './ProductItem.scss';
import { IoIosHeartEmpty, IoIosHeart} from "react-icons/io";

class ProductItem extends Component {
  render() {
    const prodData = this.props.prodData;
    let favourite = false;

    if (localStorage.getItem("favourites")) {
      favourite = JSON.parse(localStorage.getItem("favourites")).includes(prodData.id);
    }  

    return (
      <div className='product'>
        <p className="product__title">{prodData.title}</p>
        <img 
          className="product__image" 
          src={prodData.img} 
          alt="product"
        />
        <div className="product__features">
          <div className="product__features__descripton">
            <p className="product__features__descripton__text">{prodData.color}</p>
            <p className="product__features__descripton__price">${prodData.price}</p>
          </div>
          <div className="product__features__actions">
            {favourite 
              ? <IoIosHeart 
                  className="product__features__actions__icon fullHeart"
                  onClick={() => this.props.toggleFav(prodData.id)}
                /> 
              : <IoIosHeartEmpty 
                  className="product__features__actions__icon emptyHeart"
                  onClick={() => this.props.toggleFav(prodData.id)}
                />
            }
            <button 
              className="product__features__actions purchase-btn"
              onClick={() => {this.props.toggleModal(prodData.id)}}     
            >Add to cart</button>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductItem;

ProductItem.propTypes = {
  prodData: PropTypes.object.isRequired,
  toggleFav: PropTypes.func.isRequired,
  toggleModal: PropTypes.func.isRequired
};