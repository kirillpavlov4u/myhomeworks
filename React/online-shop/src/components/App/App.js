import React, { Component } from 'react';
import ProductItem from '../ProductItem/ProductItem.js';
import ModalWindow from '../ModalWindow/ModalWindow.js';
import './App.scss';
import { AiOutlineShoppingCart } from 'react-icons/ai';

class App extends Component {
  constructor(){
    super();
    this.state = {
      products: [],
      favourites: JSON.parse(localStorage.getItem("favourites")) || [],
      showModal: false,
      productCart: JSON.parse(localStorage.getItem("cart")) || [],
      productOnQueue: ''
    };
  }

  async componentDidMount() {
    const response = await fetch('/productItems.json');
    const json = await response.json()

    this.setState({
      products: json
    })
  }

  toggleFav = (id) => {
    const favProds = this.state.favourites;

    if (!favProds.includes(id)) {
      favProds.push(id);
      this.setState({
        favourites: favProds
      })
    } else {
      favProds.splice(favProds.indexOf(id), 1);
      this.setState({
        favourites: favProds
      })
    }

    localStorage.setItem('favourites', JSON.stringify(this.state.favourites));
  }

  toggleModal = (id) => {
    this.setState({
      showModal: !this.state.showModal,
      productOnQueue: id || ''
    })
  }
  
  addToCart = (id) => {
    const prodCart = this.state.productCart;

    if (!prodCart.includes(id)) {
      prodCart.push(id);
      this.setState({
        productCart: prodCart,
        showModal: !this.state.showModal,
      })
    } else {
      this.setState({
        showModal: !this.state.showModal,
      })
    }

    localStorage.setItem('cart', JSON.stringify(this.state.productCart));
  }

  closeModal = (event) => {
    const target = event.target;

    if(target.parentNode.id === 'root' ||
      target.id === 'cancel' ||
      target.nodeName === 'path' ) {
        this.setState({
          showModal: !this.state.showModal,
          productOnQueue: ''
        })
      }
  }
  
  render() {
    const products = this.state.products.map(prod => (
      <ProductItem 
        key={prod.id}
        prodData={prod}
        toggleFav={this.toggleFav}
        toggleModal={this.toggleModal}
      />
    ));

    return (
      <>
        <header>
          <AiOutlineShoppingCart className='icon'/>
          <p>TheSimpleShop</p>
        </header>
        <div className="products">
          <div className="products__container">
            {products}
          </div>
        </div>
        {
          this.state.showModal ? 
          <ModalWindow 
            productOnQueue={this.state.productOnQueue}
            addToCart={this.addToCart}
            toggleModal={this.toggleModal}
            closeModal={this.closeModal}
          />
          : false
        }
      </>
    );
  }
}

export default App;