import React, { Component } from 'react';
import Button from './Button.js';
import Modal from './Modal.js';

class App extends Component {
  constructor(){
    super();
    this.state = {
      modalType: '',
      showModal: false
    }

    this.showModalWindow = this.showModalWindow.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }  

  showModalWindow(modalType) {
    this.setState({
      modalType: modalType,
      showModal: !this.state.showModal
    })
  }

  closeModal(event) {
    if (event.target.nodeName === "BUTTON" || 
        event.target.nodeName === "svg" ||
        event.target.nodeName === "path") {
      this.setState({
        modalType: '',
        showModal: !this.state.showModal
      });
      return;
    }

    if (event.target.parentNode.id === 'root') {
      this.setState({
        modalType: '',
        showModal: !this.state.showModal
      });
      return;
    } 
  }

  render() {
    const modalType = this.state.modalType;
    const modalData = {
      green: {
        header: 'This is an Info Window',
        closeButton: false,
        text: 'You\'ve just pressed an info button',
        actions: [
          <Button
            bgColor={null}
            text="Ok"
          />
        ]
      },
      red: {
        header: 'This is an Alert Window',
        closeButton: true,
        text: 'You\'ve pressed a very dangerous, the most redder of the reddest buttons in this cruel world!',
        actions: [
          <Button
            bgColor={null}
            text="Ok"
          />,
          <Button
            bgColor={null}
            text="Cancel"
          />
        ]
      },
    }

    return (
      <>
        <Button 
          bgColor='green'
          text="Open first modal"
          click={this.showModalWindow}
        />
        <Button 
          bgColor='red'
          text="Open second modal"
          click={this.showModalWindow}
        />
        {
          this.state.showModal 
          ? <Modal
              modalType={modalType}
              header={modalData[modalType].header}
              closeButton={modalData[modalType].closeButton}
              text={modalData[modalType].text}
              click={this.closeModal}
              actions={modalData[modalType].actions}
            />
          : false
        } 
      </>
    )
  }
}

export default App;
