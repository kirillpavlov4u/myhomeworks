import React, { Component } from 'react';
import { createUseStyles } from 'react-jss';

const StyledButton = ({text, bgColor, click}) => {
  const buttonStyles = createUseStyles(
    {
      btn: {
        width: '102px',
        height: '42px',
        borderRadius: "5px",
        fontSize: '15px',
        outline: 'none',
        border: 'none',
        color: 'white',
        background: bgColor || 'hsla(0, 16%, 16%, 0.20)',
        cursor: 'pointer',
        margin: '20px'
      }
    }
  );
  
  const classes = buttonStyles();
  
  return (
    <button 
      className={classes.btn}
      onClick={() => {
        if (bgColor) {
          click(bgColor)
        }
      }}    
    >{text}</button>
  )
}

class Button extends Component {
  render() {
    return (
      <StyledButton
        bgColor={this.props.bgColor}
        text={this.props.text}
        click={this.props.click}
      />
    )
  }
}

export default Button;