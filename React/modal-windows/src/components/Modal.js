import React, { Component } from 'react';
import Button from './Button.js';
import { createUseStyles } from 'react-jss';
import { BsFillXSquareFill } from 'react-icons/bs';

const mixins = {
  btn: {
    width: '102px',
    height: '42px',
    borderRadius: "5px",
    fontSize: '15px',
    outline: 'none',
    border: 'none',
    color: 'white',
    background: 'hsla(0, 16%, 16%, 0.20)',
    cursor: 'pointer'
  },
  'red': {
    backgroundColor: '#E74C3C'
  },
  'green': {
    backgroundColor: '#3FBF3F',
  }
}

const ModalWindow = (
  {
    closeButton, 
    click, 
    windowClass, 
    header, 
    text,
    actions
  }) => {
  const modalStyles = createUseStyles(
    {
      'modal-background': {
        width: '100vw',
        height: '100vh',
        backgroundColor: 'hsla(0, 16%, 16%, 0.56)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        fontfamily: 'sans',
        position: 'absolute',
        top: 0,
        left: 0
      },
      'modal-window': {
        width: '517px',
        backgroundColor: mixins[windowClass].backgroundColor,
        borderRadius: "10px"
      },
      'header': {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '25px',
        backgroundColor: 'hsla(0, 16%, 16%, 0.20)',
        borderRadius: "10px 10px 0 0",
        marginBottom: '20px'
      },
      'title': {
        color: 'white',
        fontSize: '22px',
        fontWeight: 'bold'
      },
      'icon': {
        color: 'white',
        fontSize: '22px',
        cursor: 'pointer'
      },
      'body': {
        display: 'flex',
        flexFlow: 'column nowrap',
        justifyContent: 'space-around',
        alignItems: 'center',
        padding: '25px'
      },
      'text': {
        color: 'white',
        fontSize: '15px',
        textAlign: 'center',
        width: '95%',
        lineHeight: '30px',
        marginBottom: '30px'
      },
      'btn-wrapper': {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      'btn-apply': mixins.btn,
      'btn-reject': mixins.btn    
    }
  );
  
  const classes = modalStyles();
  
  return (
    <div 
      className={classes['modal-background']}
      id="modalBackground"
      onClick={(event) => {click(event)}}
    >
      <div className={classes['modal-window']}>
        <div className={classes['header']}>
          <p className={classes['title']}>{header}</p>
          {closeButton && <BsFillXSquareFill className={classes['icon']}/>}
        </div>
        <div className={classes['body']}>
          <p className={classes['text']}>{text}</p>
          <div className={classes['btn-wrapper']}>
            {actions.map((btn, index) => (
              <div key={index}>
                {btn}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

class Modal extends Component {
  render () {
    return (
      <ModalWindow 
        header={this.props.header}
        text={this.props.text}
        windowClass={this.props.modalType}
        openModal={this.props.openModal}
        closeButton={this.props.closeButton}
        actions={this.props.actions}
        click={this.props.click}
      />
    )
  }
}

export default Modal;