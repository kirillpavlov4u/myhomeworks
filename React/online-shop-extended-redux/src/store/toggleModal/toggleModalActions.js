import A from '../actionTypes';
import { getToggleModal } from './toggleModalSelector';

export const toggleShowModal = (event) => (dispatch, getStore) => {
  const showModalState = getToggleModal(getStore());

  const target = event.target;

  const rootDiv = target.parentNode.id === 'root';
  const modalCancelBtn = target.id === 'cancel';
  const closeIcon =  target.nodeName === 'path';
  const productPurcaseBtn = target.classList.contains('purchase-btn');
  const productDeleteBtn = target.classList.contains('delete-btn');
  const modalAddToCartBtn = target.id === 'addToCart';
  const modalDeleteFromCartBtn = target.id === 'deleteFromCart';

  if (rootDiv ||
      modalCancelBtn ||
      closeIcon || 
      productPurcaseBtn ||
      productDeleteBtn) {
        dispatch({
          type: A.TOGGLE_SHOW_MODAL,
          payload: !showModalState
        })
    }
  
  if (modalAddToCartBtn) {
    dispatch({
      type: A.TOGGLE_SHOW_MODAL,
      payload: !showModalState
    })
  }

  if (modalDeleteFromCartBtn) {
    dispatch({
      type: A.TOGGLE_SHOW_MODAL,
      payload: !showModalState
    })
  }
};