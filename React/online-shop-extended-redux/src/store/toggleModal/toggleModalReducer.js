import A from '../actionTypes';
import initialStore from '../initialStore';
import { getToggleModal } from './toggleModalSelector'

export const toggleShowModalReducer = (toggleShowModal = getToggleModal(initialStore), action) => {
  switch (action.type) {
    case A.TOGGLE_SHOW_MODAL:
      return action.payload;
    default:
      return toggleShowModal;
  }
}