import A from '../actionTypes';

export const fetchProducts = (id) => async (dispatch) => {
  const response = await fetch('/productItems.json');
  const data = await response.json();

  dispatch({
    type: A.FETCH_PRODUCTS,
    payload: data
  });
};
