import A from '../actionTypes';
import initialStore from '../initialStore';
import { products } from './fetchProductsSelector'

export const productsReducer = (allProducts = products(initialStore), action) => {
  switch (action.type) {
    case A.FETCH_PRODUCTS:
      return [...allProducts, ...action.payload];
    default:
      return allProducts;
  }
}