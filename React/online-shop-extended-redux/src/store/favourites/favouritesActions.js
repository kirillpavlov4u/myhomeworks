import A from '../actionTypes';
import { getFavourites } from './favouritesSelector';

export const addToFavourites = (id) => (dispatch) => {
  dispatch({
    type: A.ADD_TO_FAVOURITES,
    payload: id
  });
};

export const deleteFromFavourites = (id) => (dispatch) => {
  dispatch({
    type: A.DELETE_FROM_FAVOURITES,
    payload: id
  });
};

export const setFavouritesStorage = () => (dispatch, getStore) => {
  if (localStorage.getItem("favourites")) {
    dispatch({
      type: A.SET_FAVOURITES_TO_LOCAL_STORAGE,
      payload: JSON.parse(localStorage.getItem("favourites"))
    })
  } else {
    localStorage.setItem('favourites', JSON.stringify([]))
    dispatch({
      type: A.SET_FAVOURITES_TO_LOCAL_STORAGE,
      payload: []
    })
  }
};

export const updateFavsLocalStorage = () => (dispatch, getStore) => {
  const favourites = getFavourites(getStore());
  localStorage.setItem('favourites', JSON.stringify(favourites))
}