const initialStore = {
  products: [],
  favourites: [],
  productCart: [],
  productOnQueue: '',
  showModal: false
}

export default initialStore;