import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import ProductList from '../components/ProductList/';
import { getCart } from '../store/cartProducts/cardProductsSelector';
import { setCartStorage } from '../store/cartProducts/cardProductsActions';
import { setFavouritesStorage } from '../store/favourites/favouritesActions';

const CartPage = (props) => {
  const dispatch = useDispatch();
  const cart = useSelector(store => getCart(store));

  const {productItems, location, toggleFavItem, toggleCartItem} = props;
  const localStorCart = cart;
  const productsToRender = [];

  useEffect(() => {
    dispatch(setCartStorage())
    dispatch(setFavouritesStorage())
  }, [dispatch]);

  for (let i = 0; i < localStorCart.length; i++) {
    productItems.forEach(prod => {
      if (prod.id === localStorCart[i]) {
        productsToRender.push(prod);
      }
    })
  }

  return (
    <ProductList
      productItems={productsToRender}
      location={location}
      toggleFavItem={toggleFavItem}
      toggleCartItem={toggleCartItem}
    />
  );
}

export default CartPage;