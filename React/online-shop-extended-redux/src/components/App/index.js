import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import './App.scss';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Header from '../Header/';
import ModalWindow from '../ModalWindow/';
import HomePage from '../../pages/HomePage';
import CartPage from '../../pages/CartPage';
import FavouritesPage from '../../pages/FavouritesPage';
import NoPageFound from '../../pages/NoPageFound';
import { products as productsSelector } from '../../store/fetchProducts/fetchProductsSelector';
import { fetchProducts } from '../../store/fetchProducts/fetchProductsActions';


const App = () => {
  const dispatch = useDispatch();
  const products = useSelector(store => productsSelector(store));

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);
  
  return (
      <Router>
        <Header/>
        <div className="products">
          <Switch>
            <Route path='/' render={(props) => (
                <HomePage 
                  {...props} 
                  productItems={products}
                />
              )} 
              exact
            />
            <Route path='/cart' render={(props) => (
              <CartPage 
                {...props} 
                productItems={products}
              />
            )}/>
            <Route path='/favourites' render={(props) => (
              <FavouritesPage 
                {...props}
                productItems={products}
              />
              )}/>
            <Route component={NoPageFound}/>
          </Switch>
        </div>
        <ModalWindow />
      </Router>
  );
}

export default App;