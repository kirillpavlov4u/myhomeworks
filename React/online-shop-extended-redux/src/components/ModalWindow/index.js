import React from 'react';
import './ModalWindow.scss';
import { BsFillXSquareFill } from 'react-icons/bs';
import { useDispatch, useSelector } from "react-redux";
import { toggleShowModal } from '../../store/toggleModal/toggleModalActions';
import { getToggleModal } from '../../store/toggleModal/toggleModalSelector';
import { getCart } from '../../store/cartProducts/cardProductsSelector';
import { getProductInQueue } from '../../store/productOnQueue/productOnQueueSelector';
import { clearProductFromQueue } from '../../store/productOnQueue/productOnQueueActions';
import { addToCart, deleteFromCart} from '../../store/cartProducts/cardProductsActions';
import { updateCartLocalStorage } from '../../store/cartProducts/cardProductsActions';
 
const ModalWindow = (props) => {
  const dispatch = useDispatch();
  const showModal = useSelector((store) => getToggleModal(store));
  const productCart = useSelector((store) => getCart(store));
  const productOnQueue = useSelector((store) => getProductInQueue(store));

  const modalheader = {
    delete: 'Delete product from Cart?', 
    add: 'Add product to the cart?'
  },
  modalBodyText = {
    delete: 'Are you shure you want to delete product from Cart?', 
    add: 'Are you shure you want to add product to Cart?'
  };

    return (
      showModal &&
      <div 
        className='modal-background'
        onClick={(event) => { 
          dispatch(toggleShowModal(event))
          dispatch(clearProductFromQueue(event))
        }}
      >
        <div className={
          'modal-window ' +
          (productCart.includes(productOnQueue) ?
          'red' :
          'green'
          )
        }>
          <div className='modal-window__header'>
            <p className='modal-window__header__title'>
              {
                (productCart.includes(productOnQueue)) ?
                modalheader.delete :
                modalheader.add
              }
              </p>
            <BsFillXSquareFill className='modal-window__header__icon' id='close'/>
          </div>
          <div className='modal-window__body'>
            <p className='modal-window__body__text'>
              {
                (productCart.includes(productOnQueue)) ?
                modalBodyText.delete :
                modalBodyText.add
              }
            </p>
            <div className='modal-window__body__btn-wrapper'>
              {
                (productCart.includes(productOnQueue)) ?
                <button 
                  id='deleteFromCart'
                  onClick={(event) => {
                    dispatch(deleteFromCart(productOnQueue))
                    dispatch(updateCartLocalStorage());
                  }}
                >Delete card</button> :
                <button
                  id='addToCart'
                  onClick={(event) => {
                    dispatch(addToCart(productOnQueue))
                    dispatch(updateCartLocalStorage());
                  }}
                >Add card</button> 
              }
              <button
                id='cancel'
              >Cancel</button>
            </div>
          </div>
        </div>
      </div>
    )
}

export default ModalWindow;
