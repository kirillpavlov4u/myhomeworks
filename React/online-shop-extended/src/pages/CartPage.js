import React from 'react';
import ProductList from '../components/ProductList/';

const CartPage = (props) => {
  const {productItems, location, toggleFavItem, toggleCartItem} = props;
  const localStorCart = JSON.parse(localStorage.getItem("productCart"));
  const productsToRender = [];

  for (let i = 0; i < localStorCart.length; i++) {
    productItems.forEach(prod => {
      if (prod.id === localStorCart[i]) {
        productsToRender.push(prod);
      }
    })
  }

  return (
    <ProductList
      productItems={productsToRender}
      location={location}
      toggleFavItem={toggleFavItem}
      toggleCartItem={toggleCartItem}
    />
  );
}

export default CartPage;