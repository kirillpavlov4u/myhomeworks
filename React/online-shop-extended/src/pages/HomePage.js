import React from 'react';
import ProductList from '../components/ProductList/';


const HomePage = (props) => {
  return (
    <ProductList
      productItems={props.productItems}
      location={props.location}
      toggleFavItem={props.toggleFavItem}
      toggleCartItem={props.toggleCartItem}
    />
  );
}

export default HomePage;