import React from 'react';
import ProductList from '../components/ProductList/';

const FavouritesPage = (props) => {
  const {productItems, location, toggleFavItem, toggleCartItem} = props;
  const localStorFavs = JSON.parse(localStorage.getItem("favourites"));
  const productsToRender = [];

  for (let i = 0; i < localStorFavs.length; i++) {
    productItems.forEach(prod => {
      if (prod.id === localStorFavs[i]) {
        productsToRender.push(prod);
      }
    })
  }

  return (
    <ProductList
      productItems={productsToRender}
      location={location}
      toggleFavItem={toggleFavItem}
      toggleCartItem={toggleCartItem}
    />
  );
}

export default FavouritesPage;