import React from 'react';

const NoPageFound = (props) => {
  return (
    <h1 style={{
      padding: '20px 0 0 20px',
      fontSize: '40px',
      paddingBottom: '100vh'
    }}>404: No page found !</h1>
  );
}

export default NoPageFound;