import React from 'react';
import PropTypes from 'prop-types';
import './ModalWindow.scss';
import { BsFillXSquareFill } from 'react-icons/bs';

const ModalWindow = (props) => {
  const {
    closeModal, 
    addToCart,
    productCart,
    productOnQueue,
  } = props;

  ModalWindow.propTypes = {
    closeModal: PropTypes.func.isRequired,
    addToCart: PropTypes.func.isRequired,
    productOnQueue: PropTypes.string.isRequired
  };

  const modalheader = {
    delete: 'Delete product from Cart?', 
    add: 'Add product to the cart?'
  },
    modalBodyText = {
      delete: 'Are you shure you want to delete product from Cart?', 
      add: 'Are you shure you want to add product to Cart?'
    },
    modalBtnText = {
      delete: 'Delete card',
      add: 'Add card'
    };

    return (
      <div 
        className='modal-background'
        onClick={(event) => closeModal(event)}
      >
        <div className={
          'modal-window ' +
          (productCart.includes(productOnQueue) ?
          'red' :
          'green'
          )
        }>
          <div className='modal-window__header'>
            <p className='modal-window__header__title'>
              {
                (productCart.includes(productOnQueue)) ?
                modalheader.delete :
                modalheader.add
              }
              </p>
            <BsFillXSquareFill className='modal-window__header__icon' id='close'/>
          </div>
          <div className='modal-window__body'>
            <p className='modal-window__body__text'>
              {
                (productCart.includes(productOnQueue)) ?
                modalBodyText.delete :
                modalBodyText.add
              }
            </p>
            <div className='modal-window__body__btn-wrapper'>
              <button
                id='addToCart'
                onClick={() => addToCart(productOnQueue)}
              >{
                (productCart.includes(productOnQueue)) ?
                modalBtnText.delete :
                modalBtnText.add
              }</button>
              <button
                id='cancel'
              >Cancel</button>
            </div>
          </div>
        </div>
      </div>
    )
}

export default ModalWindow;
