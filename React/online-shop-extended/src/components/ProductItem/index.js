import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './ProductItem.scss';
import { IoIosHeartEmpty, IoIosHeart} from "react-icons/io";

const ProductItem = (props) => {
  const {prodData, toggleFavItem, toggleCartItem} = props;
  const [favourite, setFavourite] = useState(JSON.parse(localStorage.getItem("favourites")).includes(prodData.id));
  const [productCart, deleteFromCart] = useState(JSON.parse(localStorage.getItem("productCart")).includes(prodData.id));

  ProductItem.propTypes = {
    toggleCartItem: PropTypes.func.isRequired,
    toggleFavItem: PropTypes.func.isRequired,
    prodData: PropTypes.object.isRequired
  };


  useEffect(() => {
    deleteFromCart(JSON.parse(localStorage.getItem("productCart")).includes(prodData.id))
  })
  
  return (
      <div className='product'>
        <p className="product__title">{prodData.title}</p>
        <img 
          className="product__image" 
          src={prodData.img} 
          alt="product"
        />
        <div className="product__features">
          <div className="product__features__descripton">
            <p className="product__features__descripton__text">{prodData.color}</p>
            <p className="product__features__descripton__price">${prodData.price}</p>
          </div>
          <div className="product__features__actions">
            {favourite 
              ? <IoIosHeart 
                  className="product__features__actions__icon fullHeart"
                  onClick={() => {setFavourite(!favourite); toggleFavItem(prodData.id)}}
                /> 
              : <IoIosHeartEmpty 
                  className="product__features__actions__icon emptyHeart"
                  onClick={() => {setFavourite(!favourite); toggleFavItem(prodData.id)}}
                />
            }
            {( 
                productCart
              ) ?
              <button 
                className="product__features__actions delete-btn"
                onClick={() => {deleteFromCart(!productCart); toggleCartItem(prodData.id)}}     
              >delete from cart</button> :
              <button 
                className="product__features__actions purchase-btn"
                onClick={() => {deleteFromCart(!productCart); toggleCartItem(prodData.id)}}     
              >Add to cart</button> 
            }
          </div>
        </div>
      </div>
  );
}

export default ProductItem;