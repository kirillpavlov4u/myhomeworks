import React from 'react';
import ProductItem from '../ProductItem/';

const ProductList = (props) => {
  let productsList = [];

    productsList = props.productItems.map(prod => {
      return (
        <ProductItem 
          key={prod.id}
          prodData={prod}
          location={props.location}
          toggleFavItem={props.toggleFavItem}
          toggleCartItem={props.toggleCartItem}
        />
      )
    })

  return (
    <div className="products__container">
      {
      productsList.every(item => item === undefined) ?
      <p>No products were added</p> :
      productsList
      }
    </div> 
  );
}

export default ProductList;