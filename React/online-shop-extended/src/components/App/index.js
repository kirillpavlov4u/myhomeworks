import React, { useState, useEffect } from 'react';
import ModalWindow from '../ModalWindow/';
import './App.scss';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Header from '../Header/';
import HomePage from '../../pages/HomePage';
import CartPage from '../../pages/CartPage';
import FavouritesPage from '../../pages/FavouritesPage';
import NoPageFound from '../../pages/NoPageFound';


const App = () => {
  const [products, setProducts] = useState([]);
  const [favourites, setFavourites] = useState(JSON.parse(localStorage.getItem("favourites")));
  const [productCart, setCart] = useState(JSON.parse(localStorage.getItem("productCart")));
  const [showModal, toggleShowModal] = useState(false);
  const [productOnQueue, setProductToQueue] = useState('');

  useEffect(() => {
    if (localStorage.getItem("favourites")) {
      setFavourites(JSON.parse(localStorage.getItem("favourites")))
    } else {
      setFavourites([])
      localStorage.setItem('favourites', JSON.stringify([]))
    }
  }, []);

  useEffect(() => {
    if (localStorage.getItem("productCart")) {
      setCart(JSON.parse(localStorage.getItem("productCart")))
    } else {
      setCart([])
      localStorage.setItem('productCart', JSON.stringify([]))
    }
  }, []);

  useEffect(() => {
    async function fetchData () {
      const response = await fetch('/productItems.json');
      const data = await response.json();
      setProducts(data);
    }

    fetchData();
  }, []);

  const toggleFavItem = (id) => {
    const favs = [...favourites];

    if (!favs.includes(id)) {
      favs.push(id)
    } else {
      favs.splice(favs.indexOf(id), 1)
    }
    
    setFavourites(favs)
    localStorage.setItem("favourites", JSON.stringify(favs));
  }

  const toggleCartItem = (id) => {
    const cart = [...productCart];

    if (!cart.includes(id)) {
      cart.push(id)
    } else {
      cart.splice(cart.indexOf(id), 1)
    }
    
    toggleModal();
    setCart(cart)
    localStorage.setItem("productCart", JSON.stringify(cart));
  }
  
  const toggleModal = (id) => {
    toggleShowModal(!showModal);
    setProductToQueue(id);
  }

  const closeModal = (event) => {
    const target = event.target;

    if(target.parentNode.id === 'root' ||
      target.id === 'cancel' ||
      target.nodeName === 'path' ) {
        toggleShowModal(!showModal)
        setProductToQueue('');
      }
  }
  
  return (
      <Router>
        <Header/>
        <div className="products">
          <Switch>
            <Route path='/' render={(props) => (
                <HomePage 
                  {...props} 
                  productItems={products}
                  toggleFavItem={toggleFavItem}
                  toggleCartItem={toggleModal}
                />
              )} 
              exact
            />
            <Route path='/cart' render={(props) => (
              <CartPage 
                {...props} 
                productItems={products}
                toggleFavItem={toggleFavItem}
                toggleCartItem={toggleModal}

              />
            )}/>
            <Route path='/favourites' render={(props) => (
              <FavouritesPage 
                {...props}
                productItems={products}
                toggleFavItem={toggleFavItem}
                toggleCartItem={toggleModal}

              />
              )}/>
            <Route component={NoPageFound}/>
          </Switch>
        </div>
        {
        showModal && 
        <ModalWindow 
          closeModal={closeModal}
          productOnQueue={productOnQueue}
          addToCart={toggleCartItem}
          productCart={productCart}
        />
      }
      </Router>
  );
}

export default App;