import React from 'react';
import { AiOutlineShoppingCart } from 'react-icons/ai';
import { NavLink } from 'react-router-dom';
import './Header.scss';

const Header = (props) => {
  return (
    <header>
      <div className="logo-container">
        <AiOutlineShoppingCart className='icon'/>
        <p>TheSimpleShop</p>
      </div>
      <div className='link-container'>
        <NavLink className='link-container__link' activeClassName="selected" to='/' exact>Home</NavLink>
        <NavLink className='link-container__link' activeClassName="selected" to='/cart'>Cart</NavLink>
        <NavLink className='link-container__link' activeClassName="selected" to='/favourites'>Favourites</NavLink>
      </div>
    </header>
  );
}

export default Header;