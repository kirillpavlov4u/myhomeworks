import React from 'react';
import ProductItem from '../ProductItem/';

const ProductList = (props) => {
  const {productItems, location, toggleCartItem } = props;
  let productsList = [];

    productsList = productItems.map(prod => {
      return (
        <ProductItem 
          key={prod.id}
          prodData={prod}
          location={location}
          toggleCartItem={toggleCartItem}
        />
      )
    })

  return (
    <div className="products__container">
      {
        productsList.every(item => item === undefined) ?
        <p>No products were added</p> :
        productsList
      }
    </div> 
  );
}

export default ProductList;