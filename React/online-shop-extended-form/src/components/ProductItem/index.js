import React, { useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import PropTypes from 'prop-types';
import './ProductItem.scss';
import { IoIosHeartEmpty, IoIosHeart} from "react-icons/io";
import { getFavourites } from '../../store/favourites/favouritesSelector';
import { addToFavourites, deleteFromFavourites, updateFavsLocalStorage } from '../../store/favourites/favouritesActions';
import { getCart } from '../../store/cartProducts/cardProductsSelector';
import { toggleShowModal } from '../../store/toggleModal/toggleModalActions';
import { addProductToQueue } from '../../store/productOnQueue/productOnQueueActions';

const ProductItem = (props) => {
  ProductItem.propTypes = {
    prodData: PropTypes.object.isRequired
  };

  const dispatch = useDispatch();
  const favourites = useSelector(store => getFavourites(store));
  const cartProducts = useSelector(store => getCart(store));

  const {prodData} = props;
  const [favourite, setFavourite] = useState(favourites.includes(prodData.id));
  const productCart = cartProducts.includes(prodData.id);
  
  return (
      <div className='product'>
        <p className="product__title">{prodData.title}</p>
        <img 
          className="product__image" 
          src={prodData.img} 
          alt="product"
        />
        <div className="product__features">
          <div className="product__features__descripton">
            <p className="product__features__descripton__text">{prodData.color}</p>
            <p className="product__features__descripton__price">${prodData.price}</p>
          </div>
          <div className="product__features__actions">
            {favourite 
              ? <IoIosHeart 
                  className="product__features__actions__icon fullHeart"
                  onClick={() => {
                    setFavourite(!favourite); 
                    dispatch(deleteFromFavourites(prodData.id));
                    dispatch(updateFavsLocalStorage())
                  }}
                /> 
              : <IoIosHeartEmpty 
                  className="product__features__actions__icon emptyHeart"
                  onClick={() => {
                    setFavourite(!favourite); 
                    dispatch(addToFavourites(prodData.id));
                    dispatch(updateFavsLocalStorage())
                  }}
                />
            }
            {
              productCart ?
              <button 
                className="product__features__actions delete-btn"
                onClick={(event) => {
                  dispatch(toggleShowModal(event));
                  dispatch(addProductToQueue(prodData.id));
                }}     
              >delete from cart</button> :
              <button 
                className="product__features__actions purchase-btn"
                onClick={(event) => {
                  dispatch(toggleShowModal(event));
                  dispatch(addProductToQueue(prodData.id));
                }}     
              >Add to cart</button> 
            }
          </div>
        </div>
      </div>
  );
}

export default ProductItem;