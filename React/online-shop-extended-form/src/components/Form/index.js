import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import './Form.scss';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { addUserDataAction } from '../../store/userData/actions';
import { clearCartAction, updateCartLocalStorage } from '../../store/cartProducts/cardProductsActions';
import { products } from '../../store/fetchProducts/fetchProductsSelector';
import { getCart } from '../../store/cartProducts/cardProductsSelector';

const formNumberValidation = /^\+[0-9]{2}\([0-9]{3}\)[0-9]{7}/;
const formTextValidation = /^[аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯїЇ']+$/;

const Form = (props) => {
  const dispatch = useDispatch(); 
  const cart = useSelector(store => getCart(store));
  const alLProducts = useSelector(store => products(store));

  const formik = useFormik({
    initialValues: {
      name: '',
      surname: '',
      age: '',
      deliveryAddress : '',
      phoneNumber: ''
    },
    validationSchema: Yup.object({
      name: Yup.string()
        .matches(formTextValidation, { message: 'Name must contain only cyrillic and only letters' })
        .required('Required'),
      surname: Yup.string()
        .matches(formTextValidation, { message: 'Lastname must contain only cyrillic and only letters' })
        .required('Required'),
      age: Yup.number()
        .min(18, 'You must be at least 18 to purchase on this website!')
        .max(130, 'Please, input valid age!')
        .required('Required'),
      deliveryAddress: Yup.string()
        .min(10, 'Address information provided must contain at lest 10 characters')
        .required('Required'),
      phoneNumber: Yup.string()
        .matches(formNumberValidation, { message: 'Invalid phone number format a pattern must be +38(0XX)XXXXXXX' })
        .required('Required'),
    }),
    onSubmit: (values) => {
      console.group("Order data")
        console.log(values)
        console.group("product list")
          alLProducts.forEach(prod => {
            for (let i = 0; i < cart.length; i++) {
              if (prod.id === cart[i]) {
                console.log(prod)
              }
            }     
          })
        console.groupEnd()
      console.groupEnd()

      dispatch(addUserDataAction(values));
      dispatch(clearCartAction());
      dispatch(updateCartLocalStorage());
      alert('Your order was send')
    }
  });

  return (
    <div className="form-container">
    {cart.length  ?
      <form onSubmit={formik.handleSubmit}>
      <label htmlFor="name">{(formik.touched.name && formik.errors.name) ? formik.errors.name : 'Имя'}
        <input
          id="name"
          name="name"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.name}
          placeholder='Name'
          required
        />
      </label>
      <label htmlFor="surname">{(formik.touched.surname && formik.errors.surname) ? formik.errors.surname : 'Фамилия'}
        <input
          id="surname"
          name="surname"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.surname}
          placeholder='Lastname'
          required
        />
      </label>
      <label htmlFor="age">{(formik.touched.age && formik.errors.age) ? formik.errors.age : 'Возраст'}
        <input
          id="age"
          name="age"
          type="number"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.age}
          min="18" 
          max="130"
          placeholder='18'
          required
        />
      </label>
      <label 
        htmlFor="deliveryAddress">{
          (formik.touched.deliveryAddress && formik.errors.deliveryAddress) ? 
            formik.errors.deliveryAddress : 
            'Адресс доставки'
          }
        <input
          id="deliveryAddress"
          name="deliveryAddress"
          type="text"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.deliveryAddress}
          placeholder='Address'
          required
        />
        </label>
        <label htmlFor="phoneNumber">{
          (formik.touched.phoneNumber && formik.errors.phoneNumber) ? 
            formik.errors.phoneNumber : 
            'Номер телефона'
          }
          <input
            id="phoneNumber"
            name="phoneNumber"
            type="tel"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.phoneNumber}
            placeholder='+38(0__)XXXXXXX'
            required
          />
        </label>
      <button id='submit-button' type="submit">Checkout</button>
      </form> :
      <div className="empty-cart-status">Add product(-s) to the cart to make an order</div>}
    </div>
  );
}

export default Form;