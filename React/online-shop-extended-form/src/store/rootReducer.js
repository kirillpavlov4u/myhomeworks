import { combineReducers } from "redux";
import { productsReducer } from './fetchProducts/fetchProductsReducer';
import { favouritesReducer } from './favourites/favouritesReducers';
import { productCartReducer } from './cartProducts/cardProductsReducers';
import { addProductOnQueueReducer } from './productOnQueue/productOnQueueReducers';
import { toggleShowModalReducer } from './toggleModal/toggleModalReducer';
import { addUserDataReducer } from './userData/reducer';

export default combineReducers({
  products: productsReducer,
  favourites: favouritesReducer,
  productCart: productCartReducer,
  productOnQueue: addProductOnQueueReducer,
  showModal: toggleShowModalReducer,
  userData: addUserDataReducer
});