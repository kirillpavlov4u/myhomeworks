import A from '../actionTypes';
import initialStore from '../initialStore';
import { getFavourites } from './favouritesSelector';

export const favouritesReducer = (favourites = getFavourites(initialStore), action) => {
  switch (action.type) {
    case A.ADD_TO_FAVOURITES: {
      const {payload: id} = action;

      if (!favourites.some(fav => fav === id)) {
        return [...favourites, action.payload];
      } else {
        return favourites;
      }
    }
    case A.SET_FAVOURITES_TO_LOCAL_STORAGE:
      return action.payload;
      
    case A.DELETE_FROM_FAVOURITES: {
      const {payload: id} = action;
      return favourites.filter(fav => fav !== id);
    }

    default:
      return favourites;
  }
}