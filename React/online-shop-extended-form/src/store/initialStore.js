const initialStore = {
  products: [],
  favourites: [],
  productCart: [],
  productOnQueue: '',
  showModal: false,
  userData: {}
}

export default initialStore;