import A from '../actionTypes';
import initialStore from '../initialStore';
import { getCart } from './cardProductsSelector';

export const productCartReducer = (productCart = getCart(initialStore), action) => {
  switch (action.type) {
    case A.ADD_TO_CART: {
      const {payload: id} = action;

      if (!productCart.some(prod => prod === id)) {
        return [...productCart, action.payload];
      } else {
        return productCart;
      }
    }
    case A.SET_CART_TO_LOCAL_STORAGE:
      return action.payload;
      
    case A.DELETE_FROM_CART: {
      const {payload: id} = action;
      return productCart.filter(prod => prod !== id);
    }

    case A.CLEAR_CART: {
      return [];
    }

    default:
      return productCart;
  }
}