import A from '../actionTypes';
import { getCart } from './cardProductsSelector';

export const addToCart = (id) => (dispatch) => {
  dispatch({
    type: A.ADD_TO_CART,
    payload: id
  });
};

export const deleteFromCart = (id) => (dispatch) => {
  dispatch({
    type: A.DELETE_FROM_CART,
    payload: id
  });
};

export const setCartStorage = () => (dispatch, getStore) => {
  if (localStorage.getItem("productCart")) {
    dispatch({
      type: A.SET_CART_TO_LOCAL_STORAGE,
      payload: JSON.parse(localStorage.getItem("productCart"))
    })
  } else {
    localStorage.setItem('productCart', JSON.stringify([]))
    dispatch({
      type: A.SET_CART_TO_LOCAL_STORAGE,
      payload: []
    })
  }
};

export const updateCartLocalStorage = () => (dispatch, getStore) => {
  const cart = getCart(getStore());
  localStorage.setItem('productCart', JSON.stringify(cart))
}
export const clearCartAction = () => (dispatch) => {
  dispatch({
    type: A.CLEAR_CART
  })
}