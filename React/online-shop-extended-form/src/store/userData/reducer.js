import A from '../actionTypes';
import initialStore from '../initialStore';
import { getUserData } from './selector'

export const addUserDataReducer = (userData = getUserData(initialStore), action) => {
  switch (action.type) {
    case A.ADD_USER_DATA:
      return action.payload;
    default:
      return userData;
  }
}