import A from '../actionTypes';

export const addUserDataAction = (userData) => (dispatch, getStore) => {
    dispatch({
      type: A.ADD_USER_DATA,
      payload: userData
    })
};