import A from '../actionTypes';

export const addProductToQueue = (id) => (dispatch) => {
    dispatch({
      type: A.ADD_PRODUCT_ON_QUEUE,
      payload: id
    })
}

export const clearProductFromQueue = (event) => (dispatch) => {
  const target = event.target;

  const rootDiv = target.parentNode.id === 'root';
  const modalCancelBtn = target.id === 'cancel';
  const closeIcon =  target.nodeName === 'path';
  const productPurcaseBtn = target.classList.contains('purchase-btn');
  const productDeleteBtn = target.classList.contains('delete-btn');
  const modalAddToCartBtn = target.id === 'addToCart';
  const modalDeleteFromCartBtn = target.id === 'deleteFromCart';

  if (rootDiv ||
      modalCancelBtn ||
      closeIcon || 
      productPurcaseBtn ||
      productDeleteBtn ||
      modalAddToCartBtn ||
      modalDeleteFromCartBtn) {
        dispatch({
          type: A.CLEAR_PRODUCT_FROM_QUEUE
        })
    }
}