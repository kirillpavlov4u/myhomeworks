import A from '../actionTypes';
import initialStore from '../initialStore';
import { getProductInQueue } from './productOnQueueSelector';

export const addProductOnQueueReducer = (productOnQueue = getProductInQueue(initialStore), action) => {
  switch (action.type) {
    case A.ADD_PRODUCT_ON_QUEUE:
      return action.payload;
    case A.CLEAR_PRODUCT_FROM_QUEUE:
      return '';
    default:
      return productOnQueue;
  }
}