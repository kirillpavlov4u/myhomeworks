import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import ProductList from '../components/ProductList/';
import { setFavouritesStorage } from '../store/favourites/favouritesActions';
import { getFavourites } from '../store/favourites/favouritesSelector';
import { setCartStorage } from '../store/cartProducts/cardProductsActions';

const FavouritesPage = (props) => {
  const dispatch = useDispatch();
  const favourites = useSelector(store => getFavourites(store));
  
  const { productItems, location } = props;
  const productsToRender = [];

  useEffect(() => {
    dispatch(setCartStorage())
    dispatch(setFavouritesStorage())
  }, [dispatch]);

  for (let i = 0; i < favourites.length; i++) {
    productItems.forEach(prod => {
      if (prod.id === favourites[i]) {
        productsToRender.push(prod);
      }
    })
  }

  return (
    <ProductList
      productItems={productsToRender}
      location={location}
    />
  );
}

export default FavouritesPage;