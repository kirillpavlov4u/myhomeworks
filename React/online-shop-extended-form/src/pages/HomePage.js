import React, { useEffect } from 'react';
import { useDispatch } from "react-redux";
import ProductList from '../components/ProductList/';
import { setCartStorage } from '../store/cartProducts/cardProductsActions';
import { setFavouritesStorage } from '../store/favourites/favouritesActions';


const HomePage = (props) => {
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(setCartStorage())
    dispatch(setFavouritesStorage())
  }, [dispatch]);

  return (
    <ProductList
      productItems={props.productItems}
      location={props.location}
      toggleFavItem={props.toggleFavItem}
      toggleCartItem={props.toggleCartItem}
    />
  );
}

export default HomePage;