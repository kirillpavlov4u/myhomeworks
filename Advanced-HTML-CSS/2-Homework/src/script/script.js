$(document).ready(() => {
	btnClick();
});

let MENU_DISPLAY_PROP = "flex";

$("#burgerMenuBtn").click(() => {
	btnClick()
});

function btnClick () {
	if ($(window).width() < 768) {
		hideMenu();
		
		$("#burgerMenuBtn").click(() => {
			if ($('#linksMenu').css('display') === "none") {
				$("#linksMenu").slideDown(200);
				closeMenu();
			} else {
				$("#linksMenu").slideUp(200);
				openMenu();
			}
		});
	}
}

$(window).resize(() => {
	hideMenu();

	if ($(window).width() >= 768) {
		openMenu();
		if ($('#linksMenu').css('display') === "none") {
			$('#linksMenu').css('display', MENU_DISPLAY_PROP);
		}
	}
});


function hideMenu () {
	if ($(window).width() < 768) {
		$('#linksMenu').css('display', 'none');
	}
}

function closeMenu () {
	$("#middleMenuBar").hide(300);
	$('#topMenuBar').css("transform", 'rotateZ(-45deg)');
	$('#topMenuBar').css("top", '1.7px');
	$('#bottomMenuBar').css("transform", 'rotateZ(45deg)');
	$('#bottomMenuBar').css("top", '-1.7px');
}

function openMenu () {
	$('#topMenuBar').css("transform", 'rotateZ(0)');
	$('#topMenuBar').css("top", '0');
	$('#bottomMenuBar').css("transform", 'rotateZ(0)');
	$('#bottomMenuBar').css("top", '0');
	setTimeout(showMiddleBar, 400);
	
	function showMiddleBar () {
		$("#middleMenuBar").show(100)
	}
}